# nlp-job-ads

Using Natural Language Processing (NLP) on job ads for applications in Econometrics.

[![license][license-badge]][license] [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.6587385.svg)](https://doi.org/10.5281/zenodo.6587385)

## Table of contents

- [nlp-job-ads](#nlp-job-ads)
  - [Table of contents](#table-of-contents)
  - [Installation](#installation)
    - [1 - Setup on your local computer](#1---setup-on-your-local-computer)
    - [2 - Setup on the sciCORE cluster](#2---setup-on-the-scicore-cluster)
    - [3 - Testing the installation](#3---testing-the-installation)
    - [4 - Extra steps to use zero-shot learning functionality](#4---extra-steps-to-use-zero-shot-learning-functionality)
  - [Usage examples](#usage-examples)
    - [1 - Using the fine-tuned models in Python scripts](#1---using-the-fine-tuned-models-in-python-scripts)
    - [2 - Using the fine-tuned models on JSON batches of job ads](#2---using-the-fine-tuned-models-on-json-batches-of-job-ads)
    - [3 - Using zero-shot learning in Python scripts](#3---using-zero-shot-learning-in-python-scripts)
  - [Contributing](#contributing)
  - [Acknowledgements](#acknowledgements)

## Installation

The instructions below assume you have the [conda][conda] environment management system installed and running for your user. Those who intend to develop functionality for this package should refer to the installation instructions in [CONTRIBUTING][contributing].

### 1 - Setup

Open a Terminal window, clone this repository, and navigate to the directory where it was cloned. Run

```sh
conda env create -f environment.yml
```

to create a new conda environment named `nlp-job-ads`.  

Then, [install PyTorch](https://pytorch.org/get-started/locally/), following the instructions for you system. 

Finally, run

```bash
pip install -e .
```

from within the `nlp-job-ads` conda environment.

### 2 - Testing the installation

If the installation was successful, the commands

```python
from nlp_job_ads import utils
utils.get_edu_names("de")
```

should output

```bash
['keine Angabe', 'keine Ausbildungserfordernisse', 'OS', 'Matura', 'Berufsausbildung', 'Höhere Berufsausbildung', 'FH-Bachelor', 'UH-Bachelor', 'FH-Master', 'UH-Master', 'Berufsbegleitende Nachdiplomausbildung', 'Doktorat oder äquivalent', 'EDK-Lehrdiplom + PH-Bachelor', 'EDK-Diplom Logopädie + PH-Bachelor', 'EDK-Diplom Sonderpädagogik + PH-Master', 'EDK-Lehrdiplom + PH-Master und Äquivalente', 'EDK-Lehrdiplom/Upper Secondary School Teaching Diploma + FH- und/oder UH-Master']
```

### 3 - (Optional) Extra steps to use zero-shot learning functionality

If you want to use the zero-shot learning methods implemented in [`nlp_job_ads.predict`][predict], there is one further setup step to complete. You need to visit the [protobuf repository](https://github.com/protocolbuffers/protobuf), and follow the instructions [here](https://github.com/protocolbuffers/protobuf/tree/master/python#installation) to install the `protobuf` package **within the `nlp-job-ads` conda environment**.

## Usage examples

For an interactive usage example, see [notebooks/demo.ipynb](notebooks/demo.ipynb).

### 1 - Using the fine-tuned models in Python scripts

Fine-tuned models can be instantiated from the `nlp_job_ads.predict` model. Here's an example of using `GBERTBaseEduRedux`, a German BERT-like model, fine-tuned on an annotated dataset of reduced education requirement labels.

```sh
>>> from nlp_job_ads import predict
>>>
>>> # Instantiate classification object
>>> edu_classifier = predict.GBERTBaseEduRedux()
>>>
>>> # Define a sample text
>>> sample_text = ["Ihre Qualifikationen: Wir richten uns an eine hilfsbereite, motivierte und pflichtbewusste Persönlichkeit mit fröhlichem Erscheinungsbild, gepflegtem Auftreten und effizienter Arbeitsweise. Sie verfügen über mehrere Jahre Berufserfahrung als Koch/Köchin ? "]
>>>
>>> # Get classification scores (from 0 to 1)
>>> scores = edu_classifier.classify(sample_text)
>>> print("\n".join(["{0: >31} : {1}".format(key, val) for key, val in scores[0].items()]))
                       Bachelor : 0.005007591564208269
               Berufsausbildung : 0.9807913303375244
       Doktorat oder äquivalent : 0.0052925231866538525
        Höhere Berufsausbildung : 0.0097604775801301
                         Master : 0.003481765976175666
                      Sonstiges : 0.03316033259034157
 keine Ausbildungserfordernisse : 0.16235078871250153
```

### 2 - Using the fine-tuned models on JSON batches of job ads

If you want to run predictions for a batch of x28 job ads represented by JSON files, you can employ the script `scripts/prediction/predict_x28_edu_redux.py`. Running

```sh
python scripts/prediction/predict_x28_edu_redux.py --help
```

lets you know how to use it.

However, this script is rather computationally intensive. Unless you have access to GPUs on your local machine you'll likely be resorting to a high-performance computing infrastructure like the sciCORE cluster. The SLURM scripts `slurm/predict_x28_edu_redux_base_model.sh` and `slurm/predict_x28_edu_redux_large_model.sh` can be used as a template to that end.

One difference between running predictions locally and via SLURM script is that the cluster jobs have no access to the Internet. In particular, they cannot download the fine-tuned models at runtime. This means you'll have to preload the relevant model to your sciCORE user space before running the SLURM job. You can do that with help from the script `scripts/misc/preload_hf_classifier.py`. For example, if you want to use the model [`gonzpen/gbert-large-ft-edu-redux`](https://huggingface.co/gonzpen/gbert-large-ft-edu-redux), you may run

```sh
python scripts/misc/preload_hf_classifier.py --name gonzpen/gbert-large-ft-edu-redux --save_dir $HOME/models/nlp-job-ads/gonzpen/gbert-large-ft-edu-redux
```

and the relevant files will be stored under the path specified by the flag `--save_dir`. You may preload the model [`gonzpen/gbert-base-ft-edu-redux`](https://huggingface.co/gonzpen/gbert-base-ft-edu-redux) similarly. Once the model files are downloaded, you should point your SLURM script to them via the `model_path` variable. See how in `slurm/predict_x28_edu_redux_large_model.sh` this variable is set to `'$HOME/models/nlp-job-ads/gonzpen/gbert-large-ft-edu-redux'`.

Other variables that you will need to set in the `slurm/predict_x28_edu_redux_*` scripts are `x28_dir` and `save_path`. The variable `x28_dir` should point to the directory where the JSON files requested from the x28 API are saved. The variable `save_path` points to where the output prediction table should be saved. The output table is a `.csv` file with a column for each of the education redux labels and a row for each of the job ads represented in the JSON files in `x28_dir`.

*Remark:* you should also replace the placeholders `<your-scicore-pi-account>` and `<your-email-address>` in any SLURM script that you wish to use by your sciCORE PI account and your email address, respectively. You can find the name of your scicore PI account by running

```
echo $HOME
```

and checking the name of the parent folder to your home directory.

### 3 - Using zero-shot learning in Python scripts

Zero-shot learning means getting a model to directly do something that it was not explicitly trained to do, without further training. In our context, this means providing a text, along with a list of potential labels, and having the language model decide which label is more coherent with the context defined by the input text. An example usage of the zero-shot learning functionality in [nlp_job_ads.predict][predict] is the following. In principle, any zero-shot classification model from the [huggingface hub](https://huggingface.co/models?pipeline_tag=zero-shot-classification) can be specified under `model_name`.

```sh
>>> from nlp_job_ads import predict
>>>
>>> # Instantiate the classification object
>>> edu_classifier = predict.ZeroShotEduRedux(
      model_name="vicgalle/xlm-roberta-large-xnli-anli",
      language="de"
    )
>>>
>>> # Define a sample text
>>> sample_text = ["Ihre Qualifikationen: Wir richten uns an eine hilfsbereite,  motivierte und pflichtbewusste Persönlichkeit mit fröhlichem Erscheinungsbild, gepflegtem Auftreten und effizienter Arbeitsweise. Sie verfügen über mehrere Jahre Berufserfahrung als Koch/Köchin ?"]
>>>
>>> # Get classification scores (from 0 to 1)
>>> scores = edu_classifier.classify(sample_text)
>>> print("\n".join(["{0: >31} : {1}".format(key, val) for key, val in scores[0].items()]))
                       Bachelor : 0.14503833651542664
               Berufsausbildung : 0.9216018319129944
       Doktorat oder äquivalent : 0.09373395144939423
        Höhere Berufsausbildung : 0.22533904016017914
                         Master : 0.11937563866376877
                      Sonstiges : 0.000890037277713418
 keine Ausbildungserfordernisse : 0.0008997090626507998
```

## Contributing

See the guidelines in [CONTRIBUTING][contributing].

## Acknowledgements

The development in this repository is a result of a joint project between [CeDA][ceda] and [Conny Wunsch][conny-wunsch], Professor of Labor Economics at the University of Basel. Professor Wunsch is interested in how are firms responding to changes in the labor market. In particular, she plans on conducting an intervention study in which students are told what skill/education is required in the job market and then are checked on how they adapt their career path. In order to conduct this study, Prof. Wunsch needed a mapping from job ads to skill/education requirements.

[ceda]: https://ceda.unibas.ch/
[conda]: https://docs.conda.io/en/latest/
[config]: scripts/training/train_config.py
[conny-wunsch]: https://wwz.unibas.ch/en/faculty/people-and-areas/wunsch-conny-labour-economics/prof-dr-conny-wunsch/
[contributing]: CONTRIBUTING.md
[huggingface]: https://huggingface.co/
[license]: LICENSE
[license-badge]: https://img.shields.io/badge/license-BSD-green
[predict]: src/nlp_job_ads/predict.py
[train]: scripts/training/train.py
[x28]: https://www.x28.ch/en/our-services/
