# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- HTML documentation built with Sphinx documentation

## [1.0.0] - 2021.11.2021

Major update

### Added

- `setup.cfg`, `pyproject.toml` and `MANIFEST.in` to configure package build with setuptools
- `examples/` folder to example snippets and notebooks

### Changed

- Repository was restructured to work as a Python package
- Moved `playground.ipynb` to examples folder
- `environment.yml` is now barebone, installing only python==3.9
- Installation of `nlp_job_ads` package for development is specified by setuptools and executed done now via `pip install --editable .`
- One may also build static distributions with `pip install --upgrade build`
- Modules (`utils.py`, `predict.py`, etc.) are now imported like `from nlp_job_ads import <module>`
- README reflects changes

## [0.1.0] - 2021-11-18

### Added

- Scripts to predict education labels for all x28 JSON files within a folder
- Scripts to evaluate a classification pipeline given its name and input test data
- Method to select among the classifiers in `predict.py` via a name string

### Changed

- Output of classify methods in predict.py now consists of a list of dictionaries. One element per sample, one key per label. Dictionary values are the classification scores

### Fixed

- Bug in SequenceClassification classes that ignored the setting of the `device` argument

## [0.0.1] - 2021-11-18

### Added

- BSD 3-Clause "New" or "Revised" License
- `docs/` to keep important documentation
- `legacy/` to keep legacy code
- `models/` to keep machine learning models
- `scripts/` to keep standalone Python scripts
- `slurm/` to keep SLURM scripts to be run in the sciCORE cluster
- `README.md` to explain how to set up the environment and give some usage examples
- `config.py` to keep configuration parameters for neural network training
- `dataset.py` to keep functionality for loading, saving, and preprocessing data
- `environment.yml` to explicit the necessary packages to conda
- `metrics.py` to keep functionality relating to machine learning evaluation metrics
- `playground.ipynb` to keep an interactive record of the repository's functionality
- `predict.py` to keep functionality relating to machine learning prediction
- `train.py`, the main script to run when training a neural network
- `utils.py` to keep miscellaneous functionality

[Unreleased]: https://git.scicore.unibas.ch/ceda/nlp-job-ads/-/compare/v1.0.0...master?from_project_id=1701
[1.0.0]: https://git.scicore.unibas.ch/ceda/nlp-job-ads/-/tags/v1.0.0
[0.1.0]: https://git.scicore.unibas.ch/ceda/nlp-job-ads/-/tags/v0.1.0
[0.0.1]: https://git.scicore.unibas.ch/ceda/nlp-job-ads/-/tags/v0.0.1
