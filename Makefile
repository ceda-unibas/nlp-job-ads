NB = $(sort $(wildcard notebooks/*.ipynb))
SPHINX_DIR = docs/sphinx
SPHINX_BUILD = $(SPHINX_DIR)/_build
.PHONY: help install install-dev clean lint

help:
	@echo "install       Install package with basic dependencies"
	@echo "install-dev   Install package with development dependencies"
	@echo "clean         Remove old distribution files"
	@echo "lint          Check code style"

install:
	pip install --editable .

install-dev:
	pip install --editable .\[dev]

clean:
	git clean -Xdf
	jupyter nbconvert --inplace --ClearOutputPreprocessor.enabled=True $(NB)

lint:
	flake8
