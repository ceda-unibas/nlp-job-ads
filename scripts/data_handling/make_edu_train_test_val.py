import click
import traceback
import os

import numpy as np
import pandas as pd

from nlp_job_ads import dataset
from tqdm import tqdm

from iterstrat.ml_stratifiers import MultilabelStratifiedShuffleSplit
from sklearn.model_selection import train_test_split


@click.command()
@click.option('--xlsx_file',
              required=True,
              type=click.Path(exists=True, dir_okay=False),
              help='Path to the `.xlsx` file containing the annotated data')
@click.option('--save_dir',
              required=True,
              type=click.Path(dir_okay=True, exists=True),
              help='Path to the directory where to save the training,' +
              ' testing and validation data tables. These tables will be' +
              ' saved as `train.csv`, `test.csv`, and `val.csv`, respectively')
@click.option('--train_ratio',
              required=False,
              type=click.FloatRange(0., 1.),
              default=0.70,
              help='Size ratio of the training data in relation to the full' +
              ' dataset. Default is 0.7')
@click.option('--test_ratio',
              required=False,
              type=click.FloatRange(0., 1.),
              default=0.20,
              help='Size ratio of the test data in relation to the full' +
              ' dataset. Default is 0.2')
@click.option('--val_ratio',
              required=False,
              type=click.FloatRange(0., 1.),
              default=0.10,
              help='Size ratio of the validation data in relation to the' +
              ' full dataset. Default is 0.1')
@click.option('--random_state',
              required=False,
              type=int,
              default=2022,
              help='Controls the shuffling applied to the data before' +
              ' applying the split. Used to get reproducible results')
@click.option('--iterative_stratification',
              required=False,
              type=bool,
              default=False,
              help='Use iterative stratification to ensure that' +
              'multilabel vectors are stratified according to the unique' +
              'label dimensions, not the vectors themselves')
@click.option('--edu_redux',
              required=False,
              type=bool,
              default=False,
              help='Use reduced education label set')
def main(xlsx_file,
         save_dir,
         train_ratio,
         test_ratio,
         val_ratio,
         random_state,
         iterative_stratification,
         edu_redux):
    # Put path on a list to prepare the ground for the future, in case we want
    # to pass a list of files to open
    file_paths = [xlsx_file]

    train = {'id': [], 'text': [], 'labels': []}
    test = {'id': [], 'text': [], 'labels': []}
    val = {'id': [], 'text': [], 'labels': []}
    for path in tqdm(file_paths):
        # Load and parse the original Excel file into a pandas DataFrame
        annotated_dataset = dataset.AnnotatedEdu(path=path,
                                                 edu_redux=edu_redux)

        # Extract relevant columns as lists
        ids = annotated_dataset.get_ids()
        texts = annotated_dataset.get_texts()
        labels = annotated_dataset.get_labels()

        # Train/test/val split
        rest_size = 1 - train_ratio
        test_size = test_ratio/(test_ratio + val_ratio)

        if iterative_stratification:
            # Split data into train/rest
            msss = MultilabelStratifiedShuffleSplit(n_splits=1,
                                                    test_size=rest_size,
                                                    random_state=random_state)

            # The actual split only needs a dummy feature matrix
            X = np.zeros(len(labels))

            for train_index, rest_index in msss.split(X, np.array(labels)):
                train_id = [ids[idx] for idx in train_index]
                train_text = [texts[idx] for idx in train_index]
                train_labels = [labels[idx] for idx in train_index]

                rest_id = [ids[idx] for idx in rest_index]
                rest_text = [texts[idx] for idx in rest_index]
                rest_labels = [labels[idx] for idx in rest_index]

            # Split rest into test/val
            msss = MultilabelStratifiedShuffleSplit(n_splits=1,
                                                    test_size=test_size,
                                                    random_state=random_state)

            # Dummy feature matrix again
            X = np.zeros(len(rest_labels))

            for val_index, test_index in msss.split(X, np.array(rest_labels)):
                val_id = [rest_id[idx] for idx in val_index]
                val_text = [rest_text[idx] for idx in val_index]
                val_labels = [rest_labels[idx] for idx in val_index]

                test_id = [rest_id[idx] for idx in test_index]
                test_text = [rest_text[idx] for idx in test_index]
                test_labels = [rest_labels[idx] for idx in test_index]

        else:
            # Split data into train/rest
            train_rest_splitting = train_test_split(ids,
                                                    texts,
                                                    labels,
                                                    test_size=rest_size,
                                                    random_state=random_state)

            # Split rest into test/val
            test_val_splitting = train_test_split(train_rest_splitting[1],
                                                  train_rest_splitting[3],
                                                  train_rest_splitting[5],
                                                  test_size=test_size,
                                                  random_state=random_state)

            # Divide splitting results into train/test/val relevant lists
            train_id = train_rest_splitting[0]
            train_text = train_rest_splitting[2]
            train_labels = train_rest_splitting[4]

            val_id = test_val_splitting[0]
            val_text = test_val_splitting[2]
            val_labels = test_val_splitting[4]

            test_id = test_val_splitting[1]
            test_text = test_val_splitting[3]
            test_labels = test_val_splitting[5]

        # Concatenate splitting lists with their existing peers within the
        # train, test, and val dictionaries
        train['id'] += train_id
        train['text'] += train_text
        train['labels'] += train_labels

        val['id'] += val_id
        val['text'] += val_text
        val['labels'] += val_labels

        test['id'] += test_id
        test['text'] += test_text
        test['labels'] += test_labels

    print("***** Saving output tables *****")

    # Turn lists of dictionaries into `pandas.DataFrames`
    train_df = pd.DataFrame.from_dict(train)
    test_df = pd.DataFrame.from_dict(test)
    val_df = pd.DataFrame.from_dict(val)

    # Save data frames as `.csv` files
    train_df.to_csv(os.path.join(save_dir, 'train.csv'), index=False)
    test_df.to_csv(os.path.join(save_dir, 'test.csv'), index=False)
    val_df.to_csv(os.path.join(save_dir, 'val.csv'), index=False)


if __name__ == "__main__":
    try:
        main()
    except Exception:
        print("***** ERROR *****")
        print(traceback.format_exc())
