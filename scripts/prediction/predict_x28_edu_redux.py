import click
import traceback
import os

import pandas as pd

from nlp_job_ads import dataset
from nlp_job_ads import predict
from nlp_job_ads import utils
from tqdm import tqdm


@click.command()
@click.option('--x28_dir',
              required=True,
              type=click.Path(dir_okay=True, exists=True),
              help='Path to the directory containing the input JSON x28 files')
@click.option('--save_path',
              required=True,
              type=click.Path(dir_okay=False, exists=False),
              help='Path (with .csv extension) where to save the output table')
@click.option('--model_path',
              required=False,
              type=click.Path(dir_okay=True, exists=True),
              help="Path to preloaded model files, optional." +
              " By default, 'gonzpen/gbert-large-ft-edu-redux'" +
              " will be loaded from the huggingface hub (requires" +
              " Internet connection)",
              default=None)
@click.option('--language',
              required=False,
              type=str,
              help="Language of the labels, optional." +
              " Default is 'de'. Only used if `model_path` is" +
              " specified",
              default='de')
@click.option('--large',
              required=False,
              type=bool,
              help="Use large model for predictions, optional." +
              " Default is True. Only used when `model_path` is" +
              " not specified",
              default=True)
@click.option('--device',
              required=False,
              type=int,
              help='Device ordinal for CPU/GPU supports. Setting this to -1' +
              ' will leverage CPU, a positive integer will run the model on' +
              ' the associated CUDA device id. Default is -1',
              default=-1)
@click.option('--num_workers',
              required=False,
              type=int,
              help='Number of subprocesses to use for data loading. Set this' +
              ' to the number of CPU cores on which the training will run.' +
              'Default is 8.',
              default=8)
def main(x28_dir, save_path, model_path, language, large, device, num_workers):
    """Predict education requirements (redux) from job ads

    This script first runs through all the x28 JSON files in a directory and
    parses them into a table containing job ad IDs and text content. Then, this
    table is run through a pipeline based on a fine-tuned German-BERT sequence
    classification model. The output is a table with one column per education
    label, one row per job ad in the indicated x28 directory. The cells in the
    output table contain the model's classification scores for each (job ad,
    label) pair. The rows are indexed by x28 IDs, so that the classification
    scores can be cross-referenced with the x28 database. The output table is
    then saved to disk with comma-separated values (CSV) using the provided
    save path.
    """

    # Get names of JSON files to load
    file_names = utils.get_file_names_with_ext(path=x28_dir, ext='.json')
    file_paths = [os.path.join(x28_dir, name) for name in file_names]

    # Instantiate prediction pipeline
    try:
        if model_path is None:
            if large:
                edu_classifier = predict.GBERTLargeEduRedux(device=device)
            else:
                edu_classifier = predict.GBERTBaseEduRedux(device=device)
        else:
            edu_classifier = predict.EduReduxClassifier(model_name=model_path,
                                                        language=language,
                                                        device=device)

    except AssertionError as err:
        print("***** ERROR *****")
        print(err)
        print("Consider setting `--device '-1'` or running the script on" +
              " a CUDA-enabled system")
        return -1

    # It is faster to populate a list of dictionaries with the results, then
    # create a pandas.DataFrame once in the end.
    dictionary_list = []

    # We will need a list of the job ad IDs in the directory to index the
    # output data frame
    ids = []

    # Loop over JSON files
    print("***** Looping through JSON files *****")
    for path in tqdm(file_paths):
        # Get full file path
        json_path = os.path.join(x28_dir, path)

        # Instantiate X28 object to handle the JSON file
        job_ads = dataset.X28(path=json_path)

        # Extract IDs and texts
        ids += job_ads.get_ids()
        texts = job_ads.get_texts()

        # Get education label scores for each text
        file_scores = edu_classifier.classify(texts, num_workers=num_workers)

        # Concatenate results to the dictionary list
        dictionary_list += file_scores

    print("***** Saving output data frame *****")
    # Transform output into a data frame
    df = pd.DataFrame.from_dict(dictionary_list)

    # Insert x28 IDs as a new column and set it as index
    df['id'] = ids
    df = df.set_index('id')

    # Save table
    df.to_csv(save_path)


if __name__ == "__main__":
    try:
        main()
    except Exception:
        print("***** ERROR *****")
        print(traceback.format_exc())
