import os

HOME = os.environ['HOME']

###########
# Dataset #
###########

# Path to the CSV dataset used for hyperparameter search. The table should
# contain at least two columns, 'text' and 'labels', which are used in the
# procedure
TRAIN_DATA_FILE = os.path.join(HOME,
                               "data/x28-job-ads/annotated/edu-reqs/train.csv")
VAL_DATA_FILE = os.path.join(HOME,
                             "data/x28-job-ads/annotated/edu-reqs/val.csv")

# Number of distinct labels in the dataset
NUM_LABELS = 17

#########
# Model #
#########

# Name of the pretrained model from huggingface. It should be either a name
# that points to a model in the huggingface hub, or a local path containing
# preloaded model files
MODEL_NAME = os.path.join(
    HOME,
    "models/sequence-classification/deepset/gbert-base-17"
    )

##########
# Output #
##########

# Path where to save the fine-tuned model and auxiliary files
SAVE_DIR = os.path.join(
    HOME,
    "models/nlp-job-ads/gbert-base-ft-edu/hp-search"
    )

################################
# `transformers` configuration #
################################

# Level of verbosity from `transformers` library. Options are "debug", "info",
# "warning", "error", "critical"
TRANSFORMERS_VERBOSITY = "warning"

# Set tokenizer parallelism to "false" to disable a warning that comes up from
# instantiating the tokenizer before training
TOKENIZERS_PARALLELISM = "false"

########################################
# `transformers.Trainer` configuration #
########################################

# Number of steps after which evaluation is done and logged
EVAL_STEPS = 100

# Number of subprocesses to use for data loading (PyTorch only). Set this to
# the number of CPU cores on which the training will run
DATALOADER_NUM_WORKERS = 4

# Use in conjunction with LOAD_BEST_MODEL_AT_END to specify the metric to use
# to compare two different models. Must be the name of a metric returned by the
# evaluation with or without the prefix "eval_". Use either "loss", or the name
# by COMPUTE_METRICS.
METRIC_FOR_BEST_MODEL = 'lrap'

# Use in conjunction with LOAD_BEST_MODEL_AT_END and METRIC_FOR_BEST_MODEL to
# specify if better models should have a greater metric or not
GREATER_IS_BETTER = True

# Whether you want to pin memory in data loaders or not
DATALOADER_PIN_MEMORY = True

# Evaluation metric to use when training. See function `get_scorer()` in
# `metrics.py` for options
COMPUTE_METRICS = 'lrap'

# When evaluating, weigh samples according to class frequencies (True) or not
# (False)
WEIGH_SAMPLES = True

##############################################################
# `transformers.Trainer.hyperparameter_search` configuration #
##############################################################

# The number of trial runs to test.
N_TRIALS = 100

###############################
# Hyperparameter space limits #
###############################

# Minimum and maximum batch sizes per device. The actual batch sizes used in
# the search will be the sequence of powers of two between the largest power of
# two smaller than or equal to MIN_BATCH_SIZE and the largest power of two
# smaller than or equal to MAX_BATCH_SIZE
MIN_BATCH_SIZE = 2
MAX_BATCH_SIZE = 8

# Minimum and maximum learning rates
MIN_LR = 1e-5
MAX_LR = 1e-2

# Minimum and maximum weight decay
MIN_WEIGHT_DECAY = 1e-5
MAX_WEIGHT_DECAY = 1e-2

# Minimum and maximum number of training epochs
MIN_EPOCHS = 1
MAX_EPOCHS = 32

# Minimum and maximum seeds
MIN_SEED = 2020
MAX_SEED = 2022
