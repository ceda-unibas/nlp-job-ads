import os
import traceback

import pandas as pd
import train_config as config

from nlp_job_ads import dataset
from nlp_job_ads import metrics
from transformers import (
    AutoTokenizer,
    AutoModelForSequenceClassification,
    TrainingArguments,
    Trainer,
    set_seed
)
from transformers.trainer_utils import get_last_checkpoint

# Set verbosity level from transformers side
os.environ["TRANSFORMERS_VERBOSITY"] = config.TRANSFORMERS_VERBOSITY

# Set tokenizer parallelism
os.environ["TOKENIZERS_PARALLELISM"] = config.TOKENIZERS_PARALLELISM


def save_data(ids: list, texts: list, labels: list, path: str) -> None:
    df = pd.DataFrame.from_dict({'id': ids,
                                 'text': texts,
                                 'labels': labels})
    df.to_csv(path, index=False)


def fine_tune():
    """Fine tune a pretrained `huggingface` classifier"""

    # Set seed for reproducibility
    set_seed(config.SEED)

    # Load train/test labeled data from CSV files
    train_texts_labels = dataset.load_ft_extract(config.TRAIN_DATA_FILE)
    train_texts = list(train_texts_labels['text'])
    train_labels = list(train_texts_labels['labels'])

    test_texts_labels = dataset.load_ft_extract(config.TEST_DATA_FILE)
    test_texts = list(test_texts_labels['text'])
    test_labels = list(test_texts_labels['labels'])

    # Define tokenizer from pretrained model
    tokenizer = AutoTokenizer.from_pretrained(config.MODEL_NAME)

    # Tokenize dataset splits
    train_encodings = tokenizer(train_texts,
                                truncation=True,
                                padding=True)
    test_encodings = tokenizer(test_texts,
                               truncation=True,
                               padding=True)

    # Define tokenized datasets as custom PyTorch Dataset instances
    train_dataset = dataset.SequenceClassification(train_encodings,
                                                   train_labels)
    test_dataset = dataset.SequenceClassification(test_encodings,
                                                  test_labels)

    # Define pretrained model and specify the number of labels in the new task
    model = AutoModelForSequenceClassification.from_pretrained(
        config.MODEL_NAME,
        num_labels=config.NUM_LABELS
        )

    # Instantiate a `TrainingArguments` object, specifying the settings of the
    # training procedure
    training_args = TrainingArguments(
        output_dir=config.SAVE_DIR,
        evaluation_strategy=config.EVAL_STRATEGY,
        eval_steps=config.EVAL_STEPS,
        per_device_train_batch_size=config.PER_DEVICE_TRAIN_BATCH_SIZE,
        per_device_eval_batch_size=config.PER_DEVICE_EVAL_BATCH_SIZE,
        learning_rate=config.LEARNING_RATE,
        weight_decay=config.WEIGHT_DECAY,
        adam_beta1=config.ADAM_BETA1,
        adam_beta2=config.ADAM_BETA2,
        adam_epsilon=config.ADAM_EPSILON,
        max_grad_norm=config.MAX_GRAD_NORM,
        num_train_epochs=config.NUM_TRAIN_EPOCHS,
        max_steps=config.MAX_STEPS,
        lr_scheduler_type=config.LR_SCHEDULER_TYPE,
        warmup_ratio=config.WARMUP_RATIO,
        logging_strategy=config.LOGGING_STRATEGY,
        logging_steps=config.LOGGING_STEPS,
        save_strategy=config.SAVE_STRATEGY,
        save_steps=config.SAVE_STEPS,
        no_cuda=config.NO_CUDA,
        seed=config.SEED,
        dataloader_num_workers=config.DATALOADER_NUM_WORKERS,
        load_best_model_at_end=config.LOAD_BEST_MODEL_AT_END,
        metric_for_best_model=config.METRIC_FOR_BEST_MODEL,
        greater_is_better=config.GREATER_IS_BETTER,
        dataloader_pin_memory=config.DATALOADER_PIN_MEMORY
        )

    # Define evaluation metric
    compute_metrics = metrics.get_scorer(config.COMPUTE_METRICS,
                                         config.WEIGH_SAMPLES)

    # Instantiate a `Trainer` object
    trainer = Trainer(model=model,
                      args=training_args,
                      train_dataset=train_dataset,
                      eval_dataset=test_dataset,
                      tokenizer=tokenizer,
                      compute_metrics=compute_metrics)

    # Load the last checkpoint (if any)
    last_checkpoint = get_last_checkpoint(training_args.output_dir)
    if last_checkpoint is not None:
        print(
            "Checkpoint detected, resuming training at {}.".format(
                last_checkpoint
                )
            )

    # Fine-tune the pretrained model
    print("\n***** Fine-tunning *****\n")
    train_results = trainer.train(resume_from_checkpoint=last_checkpoint)
    train_metrics = train_results.metrics

    # Save
    print("\n***** Saving *****\n")

    # Save the model and tokenizer
    trainer.save_model()
    trainer.save_state()

    # Save metrics
    trainer.log_metrics("train", train_metrics)
    trainer.save_metrics("train", train_metrics)


if __name__ == "__main__":
    try:
        fine_tune()
    except Exception:
        print("***** ERROR *****")
        print(traceback.format_exc())
