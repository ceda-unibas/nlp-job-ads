import os

HOME = os.environ['HOME']

###########
# Dataset #
###########

# Path to the CSV dataset used for fine-tunning the model. The table should
# contain at least two columns, 'text' and 'labels', which are used in the
# fine-tunning procedure
TRAIN_DATA_FILE = os.path.join(HOME,
                               "data/x28-job-ads/annotated/edu-reqs/train.csv")

# Path to the CSV dataset used to test (evaluate) the model being fine-tuned.
# The table should follow the same format and the training data
TEST_DATA_FILE = os.path.join(HOME,
                              "data/x28-job-ads/annotated/edu-reqs/test.csv")

# Number of distinct labels in the dataset used for fine-tunning
NUM_LABELS = 7

#########
# Model #
#########

# Name of the pretrained model from huggingface. It should be either a name
# that points to a model in the huggingface hub, or a local path containing
# preloaded model files
MODEL_NAME = os.path.join(
    HOME,
    "models/sequence-classification/deepset/gbert-large-7"
    )

##########
# Output #
##########

# Path where to save the fine-tuned model and auxiliary files
SAVE_DIR = os.path.join(
    HOME,
    "models/nlp-job-ads/gbert-large-ft-edu-redux"
    )

################################
# `transformers` configuration #
################################

# Level of verbosity from `transformers` library. Options are "debug", "info",
# "warning", "error", "critical"
TRANSFORMERS_VERBOSITY = "warning"

# Set tokenizer parallelism to "false" to disable a warning that comes up from
# instantiating the tokenizer before training
TOKENIZERS_PARALLELISM = "false"

########################################
# `transformers.Trainer` configuration #
########################################

# Evaluation strategy to use.
EVAL_STRATEGY = "steps"

# Number of steps after which evaluation is done and logged if
# EVAL_STRATEGY == "steps"
EVAL_STEPS = 100

# Batch size per GPU/CPU/TPU core for training
PER_DEVICE_TRAIN_BATCH_SIZE = 2

# Batch size per GPU/CPU/TPU core for evaluation
PER_DEVICE_EVAL_BATCH_SIZE = 2

# Initial learning rate for the AdamW optimizer
LEARNING_RATE = 2.7e-05

# Weight decay to apply to all layers except bias and LayerNorm weights
WEIGHT_DECAY = 1e-05

# The beta1 hyperparameter for the AdamW optimizer
ADAM_BETA1 = 0.9

# The beta2 hyperparameter for the AdamW optimizer
ADAM_BETA2 = 0.999

# The epsilon hyperparameter for the AdamW optimizer
ADAM_EPSILON = 1e-8

# Maximum gradient norm (for gradient clipping)
MAX_GRAD_NORM = 1.0

# Total number of training epochs to perform
NUM_TRAIN_EPOCHS = 9

# Maximum number of training steps (overrides NUM_TRAIN_EPOCHS). Can be set to
# a negative number, so that NUM_TRAIN_EPOCHS takes precedence)
MAX_STEPS = -1

# Learning rate scheduler type. See documentation of
# `transformers.SchedulerType`: https://bit.ly/3k6S93p
LR_SCHEDULER_TYPE = "linear"

# Ratio of total training steps used for a linear warmup from 0 to
# LEARNING_RATE
WARMUP_RATIO = 0.0

# The logging strategy to adopt during training
LOGGING_STRATEGY = "steps"

# Number of update steps between two logs if LOGGING_STRATEGY = "steps"
LOGGING_STEPS = 500

# The checkpoint save strategy to adopt during training
SAVE_STRATEGY = "steps"

# Number of updates steps before two checkpoint saves if
# SAVE_STRATEGY = "steps"
SAVE_STEPS = 500

# Whether to not use CUDA even when it is available or not.
NO_CUDA = False

# Seed for reproducible behavior
SEED = 2021

# Number of subprocesses to use for data loading (PyTorch only). Set this to
# the number of CPU cores on which the training will run
DATALOADER_NUM_WORKERS = 4

# Whether or not to load the best model found during training at the end of
# training. When set to True, the parameter SAVE_STRATEGY needs to be the same
# as EVAL_STRATEGY, and in the case it is "steps", SAVE_STEPS must be a round
# multiple of EVAL_STEPS.
LOAD_BEST_MODEL_AT_END = True

# Use in conjunction with LOAD_BEST_MODEL_AT_END to specify the metric to use
# to compare two different models. Must be the name of a metric returned by the
# evaluation with or without the prefix "eval_". Use either "loss", or the name
# by COMPUTE_METRICS.
METRIC_FOR_BEST_MODEL = 'lrap'

# Use in conjunction with LOAD_BEST_MODEL_AT_END and METRIC_FOR_BEST_MODEL to
# specify if better models should have a greater metric or not
GREATER_IS_BETTER = True

# Whether you want to pin memory in data loaders or not
DATALOADER_PIN_MEMORY = True

# Evaluation metric to use when training. See function `get_scorer()` in
# `metrics.py` for options
COMPUTE_METRICS = 'lrap'

# When evaluating, weigh samples according to class frequencies (True) or not
# (False)
WEIGH_SAMPLES = True
