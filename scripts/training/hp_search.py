import transformers
import json
import optuna
import os
import traceback

import hp_search_config as config
import numpy as np

from nlp_job_ads import dataset
from nlp_job_ads import metrics
from transformers import (
    AutoTokenizer,
    AutoModelForSequenceClassification,
    TrainingArguments,
    Trainer,
)
from typing import Dict, List

# Set verbosity level from transformers side
os.environ["TRANSFORMERS_VERBOSITY"] = config.TRANSFORMERS_VERBOSITY

# Set tokenizer parallelism
os.environ["TOKENIZERS_PARALLELISM"] = config.TOKENIZERS_PARALLELISM


def save_best_run(br: transformers.trainer_utils.BestRun) -> None:
    """Save results of hyperparameter search to disk as JSON

    Parameters
    ----------
    br : transformers.trainer_utils.BestRun
        NamedTuple returned by `transformers.Trainer.hyperparameter_search`
    """
    filepath = os.path.join(config.SAVE_DIR, 'best_run.json')
    with open(filepath, "w") as f:
        f.write(json.dumps(br._asdict()))


class HyperParamSearch(object):
    """Hyperparameter search class

    Attributes and methods are determined by the configuration file
    `hp_search_config.py`
    """
    def __init__(self) -> None:
        self.model_name = config.MODEL_NAME
        self.num_labels = config.NUM_LABELS
        self.tokenizer = AutoTokenizer.from_pretrained(self.model_name)
        self.scorer = metrics.get_scorer(config.COMPUTE_METRICS,
                                         config.WEIGH_SAMPLES)
        self.num_trials = config.N_TRIALS

    def encode(self, texts: List[str]) -> List:
        """Tokenize texts

        Parameters
        ----------
        texts : List[str]
            List of texts to tokenize

        Returns
        -------
        List
            List of tokens
        """
        return self.tokenizer(texts, truncation=True, padding=True)

    def model_init(self) -> Dict:
        """Model initialization function to pass to `transformers.Trainer`

        Returns
        -------
        Dict

        """
        return AutoModelForSequenceClassification.from_pretrained(
            self.model_name,
            num_labels=self.num_labels,
            return_dict=True
            )

    def compute_metrics(
        self,
        eval_pred: transformers.trainer_utils.EvalPrediction
            ) -> metrics.Scorer:
        """Scoring function to evaluate model predictions

        Parameters
        ----------
        eval_pred : transformers.trainer_utils.EvalPrediction
            The evaluation prediction object

        Returns
        -------
        metrics.Scorer
            the scoring function
        """
        return self.scorer(eval_pred)

    def space(self, trial: optuna.trial.Trial) -> Dict:
        """Define the hyperparameter search space

        Parameters
        ----------
        trial : optuna.trial.Trial
            An `optuna` trial object

        Returns
        -------
        Dict
            The hyperparameter search space. Each key represents one
            hyperparameter. Each value, the hyperparameter's search space
        """
        min_batch_power = int(
            np.floor(np.log(config.MIN_BATCH_SIZE)/np.log(2))
            )
        max_batch_power = int(
            np.floor(np.log(config.MAX_BATCH_SIZE)/np.log(2))
            )
        space = {
            "per_device_train_batch_size": trial.suggest_categorical(
                "per_device_train_batch_size",
                [2**p for p in range(min_batch_power, max_batch_power + 1)]),
            "learning_rate": trial.suggest_float("learning_rate",
                                                 config.MIN_LR,
                                                 config.MAX_LR,
                                                 log=True),
            "weight_decay": trial.suggest_float("weight_decay",
                                                config.MIN_WEIGHT_DECAY,
                                                config.MIN_WEIGHT_DECAY,
                                                log=True),
            "num_train_epochs": trial.suggest_int("num_train_epochs",
                                                  config.MIN_EPOCHS,
                                                  config.MAX_EPOCHS),
            "seed": trial.suggest_int("seed",
                                      config.MIN_SEED,
                                      config.MAX_SEED),
        }
        return space

    def search(self,
               train_texts: List[str],
               train_labels: List,
               val_texts: List[str],
               val_labels: List,) -> transformers.trainer_utils.BestRun:
        """Search for the best hyperparameters

        Parameters
        ----------
        train_texts : List[str]
            List of training texts
        train_labels : List
            List of training labels
        val_texts : List[str]
            List of validation texts
        val_labels : List
            List of validation labels

        Returns
        -------
        transformers.trainer_utils.BestRun
            The best run named tuple. It has fields 'run_id', 'objective', and
            'hyperparameters'
        """

        # Encode texts
        train_encodings = self.encode(train_texts)
        test_encodings = self.encode(val_texts)

        # Define custom PyTorch datasets with encodings and labels
        train_dataset = dataset.SequenceClassification(train_encodings,
                                                       train_labels)
        test_dataset = dataset.SequenceClassification(test_encodings,
                                                      val_labels)

        training_args = TrainingArguments(
            output_dir=config.SAVE_DIR,
            eval_steps=config.EVAL_STEPS,
            dataloader_num_workers=config.DATALOADER_NUM_WORKERS,
            dataloader_pin_memory=config.DATALOADER_PIN_MEMORY
            )

        trainer = Trainer(
            args=training_args,
            train_dataset=train_dataset,
            eval_dataset=test_dataset,
            model_init=self.model_init,
            compute_metrics=self.compute_metrics,
            )

        if config.GREATER_IS_BETTER is True:
            direction = "maximize"
        else:
            direction = "minimize"

        best_run = trainer.hyperparameter_search(hp_space=self.space,
                                                 n_trials=self.num_trials,
                                                 direction=direction)

        return best_run


def hp_search():
    """Search for the best training hyperparameters for  a `huggingface`
    sequence classifier
    """

    # Load labeled data from a CSV file
    print("\n***** Loading data *****\n")
    train_texts_labels = dataset.load_ft_extract(config.TRAIN_DATA_FILE)
    train_texts = list(train_texts_labels['text'])
    train_labels = list(train_texts_labels['labels'])

    val_texts_labels = dataset.load_ft_extract(config.VAL_DATA_FILE)
    val_texts = list(val_texts_labels['text'])
    val_labels = list(val_texts_labels['labels'])

    # Instantiate hyperparameter search object
    print("\n***** Setting up hyperparameter search *****\n")
    hps = HyperParamSearch()

    # Search
    print("\n***** Searching *****\n")
    br = hps.search(train_texts, train_labels, val_texts, val_labels)

    # Save the best hyperparameters found
    print("\n***** Saving best run *****\n")
    save_best_run(br)


if __name__ == "__main__":
    try:
        hp_search()
    except Exception:
        print("***** ERROR *****")
        print(traceback.format_exc())
