"""Preload a model & tokenizer to disk"""

import click

from transformers import (
    AutoTokenizer,
    AutoModelForSequenceClassification,
)
from transformers import logging

logging.set_verbosity_info()


@click.command()
@click.option('--name',
              required=True,
              type=str,
              help='Name of the pretrained model from huggingface')
@click.option('--save_dir',
              required=True,
              type=click.Path(dir_okay=True, exists=False),
              help='Path where to save the model')
@click.option('--num_labels',
              required=False,
              type=int,
              default=None,
              help='Number of labels in the classifier head')
def hf_classifier(name, save_dir, num_labels):
    """
    Preload `huggingface` model and corresponding tokenizer to disk.
    """

    # Load tokenizer and model
    print("\n***** Loading *****\n")
    tokenizer = AutoTokenizer.from_pretrained(name)
    if num_labels is None:
        model = AutoModelForSequenceClassification.from_pretrained(name)
    else:
        model = AutoModelForSequenceClassification.from_pretrained(
            name,
            num_labels=num_labels
            )

    # Save tokenizer and model to the specified directory
    print("\n***** Saving *****\n")
    tokenizer.save_pretrained(save_directory=save_dir)
    model.save_pretrained(save_directory=save_dir)


if __name__ == "__main__":
    try:
        hf_classifier()
    except Exception as err:
        print("ERROR: {}".format(err))
