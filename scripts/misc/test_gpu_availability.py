import torch

if torch.has_cuda:
    print("CUDA is available: {}".format(torch.cuda.is_available()))

    print("Number of GPUs available: {}".format(torch.cuda.device_count()))

    print("Name of current device: {}".format(
        torch.cuda.get_device_name(torch.cuda.current_device())
        ))

else:
    print("Torch was not compiled with CUDA enabled")
