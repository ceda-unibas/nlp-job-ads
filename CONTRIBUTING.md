# Contributing

Contributions are welcome and appreciated. The development of this package takes place on [sciCORE's GitLab][scicore-gitlab]. Issues, bugs, and feature requests should be reported there. Code and documentation can be improved by submitting a pull request. Please add documentation for any new code snippet.

You can improve or add functionality in the repository by forking from main, developing your code, and then creating a pull request. After making changes:

- Update [README][readme] and [CHANGELOG][changelog], if applicable.
- Write tests for your developed functionality. Run `make tests` to check the test coverage. Check the generated coverage report at `htmlcov/index.html` to make sure the tests reasonably cover the changes you've introduced.
- Check the code style (linting) by running `make lint`.
- Build the Sphinx documentation with `make doc`. Check the newly-generated documentation at `docs/sphinx/_build/html/index.html`.

## Table of contents

- [Contributing](#contributing)
  - [Table of contents](#table-of-contents)
  - [Setup](#setup)
  - [Fine-tunning a new model](#fine-tunning-a-new-model)
    - [1- Fine-tunning locally](#1--fine-tunning-locally)
    - [Fine-tunning via a SLURM script on the sciCORE cluster](#fine-tunning-via-a-slurm-script-on-the-scicore-cluster)
  - [Making a release](#making-a-release)
  - [Generating distribution archives](#generating-distribution-archives)
  - [Uploading distribution archives to PyPI](#uploading-distribution-archives-to-pypi)
    - [Test upload](#test-upload)
    - [Real upload](#real-upload)
  - [Repository organization](#repository-organization)

## Setup

To set up for local development, first clone the repository.

```bash
git clone ssh://git@git.scicore.unibas.ch:2222/ceda/nlp-job-ads.git
cd nlp-job-ads
```

Then run

```bash
conda env create -f environment.yml
conda activate nlp-job-ads
```

to create a conda environment named `nlp-job-ads`. Finally, run

```bash
make install-dev
```

to install the `nlp_job_ads` package (in editable mode) and its dependencies to the newly-created conda environment. See the [Makefile][makefile] for the precise commands running in the background.

## Fine-tunning a new model

A [`huggingface`][huggingface] classifier can be fine-tuned to a particular dataset by executing the script [`train.py`][train]. Training hyper-parameters and configuration are defined in [`train_config.py`][config]. You may change the contents in the configuration file to customize the fine-tunning procedure. For example, you can specify another pre-trained `huggingface` model to use under the variable `MODEL_NAME`. Recommended models for dealing with texts in German are:

1. `deepset/gbert-base`
2. `deepset/gbert-large`
3. `deepset/gelectra-large`
4. `bert-base-german-cased`
5. `dbmdz/german-gpt2`

### 1- Fine-tunning locally

Simply run

```sh
python scripts/training/train.py
```

from within the `nlp-job-ads` environment.

### Fine-tunning via a SLURM script on the sciCORE cluster

A SLURM script running on the sciCORE cluster has no Internet access, so it cannot download a pre-trained model from [`huggingface`][huggingface] at execution time. Thus, you have to explicitly download the models that you are interested in to disk beforehand. A Python utility, [`scripts/misc/preload_hf_classifier.py`](scripts/misc/preload_hf_classifier.py) is provided to do just so. For example, run

```sh
python scripts/misc/preload_hf_classifier.py --name deepset/gbert-base --save_dir ~/models/sequence-classification/deepset/gbert-base-17 --num_labels 17
```

straight from the Terminal to have all the `deepset/gbert-base` model and tokenizer files stored in `~/models/sequence-classification/deepset/gbert-base-edu`, with a classification head prepared for a task with `17` labels.

After preloading the desired model, modify the flag `MODEL_NAME` in the [configuration file][config] to point to the directory where the model was saved.

Then, you can simply queue the SLURM script with

```sh
sbatch slurm/train.sh
```

and wait for the fine-tunning procedure to execute.

The newly-trained model should then be used through a class that inherits from `nlp_job_ads.predict.SequenceClassifier` for compatibility with other models in the repository. You may also upload your trained model to the [huggingface hub](https://huggingface.co/) to use it following the template in `nlp_job_ads.predict.GBERTBaseEduRedux`, for example.

## Making a release

1. Update the version number in [setup][setup] and [__init__.py][__init__], following [Semantic Versioning][semantic-versioning] guidelines.
2. Update [CHANGELOG][changelog] with a new section for the new release version. Remember to update the link pointing to unreleased development, in the bottom of the file.
3. Create a corresponding [git tag][tag] for the release.
4. Create a new [release on GitLab][release], pointing to the created tag.

## Generating distribution archives

Make sure you have the latest version of PyPA’s build installed:

```bash
python -m pip install --upgrade build
```

Then run the following command to clean all git-ignored files, old distribution archives and build new ones from scratch:

```bash
make dist
```

Once completed, there should be two files in the `dist/` folder:

```bash
dist
├── nlp-job-ads-x.x.x.tar.gz
└── nlp_job_ads-x.x.x-py3-none-any.whl
```

*Remark:* Instead of `x.x.x` you will see the actual version number specified in [setup][setup].

## Uploading distribution archives to PyPI

### Test upload

Before uploading to PyPI, it is advisable that you first test the upload and installation process with TestPyPI. [Register][testpypi-register] for an account and request a secure API token under [Account settings > API tokens][testpypi-api-token]. Once you have your token saved somewhere, run

```bash
make release-test
```

to upload the distribution archives to TestPyPI (you'll be asked for your API token). After a few minutes, test if you can install the uploaded package by creating a fresh conda (or virtual) environment and running

```bash
make install-test
```

Verify the contents of the project's [TestPyPI page][testpypi-project-page].

### Real upload

You may now proceed to uploading the distribution archives to PyPI. [Register][pypi-register] for an account on PyPI (different server than TestPyPI) and get an [API token][pypi-api-token] similarly to how you did it with TestPyPI. Run

```bash
make release
```

to upload the distribution archives to PyPI (you'll be asked for your API token).

You can verify if the package was successfully uploaded after a few minutes by navigating to the project's [PyPI page][pypi-project-page].

## Repository organization

```bash
nlp-job-ads
├── .gitignore          # Files ignored by the git revision control system
├── CHANGELOG.md        # Changelog
├── CONTRIBUTING.md     # Contributing guidelines
├── LICENSE             # Project license
├── MANIFEST.in         # Source distribution inclusion/exclusion file
├── README.md           # Description of repository and Python package
├── dist                # Where distribution archives are saved
├── docs                # Documentation
├── environment.yml     # Conda environment file
├── notebooks            # Where example snippets and notebooks are saved
├── htmlcov             # Where the HTML coverage report is saved
├── Makefile            # Wrappers for sequences of command-line programs
├── pyproject.toml      # File that setuptools uses to build distribution archives
├── scripts
│   └── *.py            # Standalone Python scripts
├── setup.cfg           # Setup configuration for building with setuptools
├── slurm
│   └── *.sh            # SLURM scripts to be run in the sciCORE cluster
└── src
    ├── nlp_job_ads     # Source code for the Python package
    |    └── *.py        # All the different modules
    └── tests
         └── *.py        # All the different tests, organized by module
```

[changelog]: CHANGELOG
[config]: scripts/training/train_config.py
[huggingface]: https://huggingface.co/
[Makefile]: Makefile
[__init__]: src/nlp_job_ads/__init__.py
[readme]: README.md
[release]: https://git.scicore.unibas.ch/ceda/nlp-job-ads/-/releases
[pypi-project-page]: https://pypi.org/project/nlp-job-ads
[pypi-register]: https://pypi.org/account/register/
[pypi-api-token]: https://pypi.org/account/#api-tokens
[scicore-gitlab]: https://git.scicore.unibas.ch/
[semantic-versioning]: https://semver.org/
[setup]: setup.cfg
[tag]: https://git.scicore.unibas.ch/ceda/nlp-job-ads/-/tags
[testpypi-project-page]: https://test.pypi.org/project/nlp-job-ads/
[testpypi-register]: https://test.pypi.org/account/register/
[testpypi-api-token]: https://test.pypi.org/account/#api-tokens
[train]: scripts/training/train.py
[upload-pypi]: https://packaging.python.org/guides/distributing-packages-using-setuptools/#create-an-account
