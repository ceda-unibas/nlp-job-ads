[metadata]
name = nlp-job-ads
version = 1.0.0
author = Rodrigo C. G. Pena
author_email = rodrigo.cerqueiragonzalezpena@unibas.ch
description = Using natural language processing on job ads for applications in econometrics
long_description = file: README.md, CHANGELOG.md
long_description_content_type = text/markdown
license = BSD 3-Clause License
license_files = file: LICENSE
url = https://git.scicore.unibas.ch/ceda/nlp-job-ads
project_urls =
    PyPI = https://pypi.org/project/nlp-job-ads
    Source Code = https://git.scicore.unibas.ch/ceda/nlp-job-ads
    Issue Tracker = https://git.scicore.unibas.ch/ceda/nlp-job-ads/-/issues
classifiers =
    Development Status :: 3 - Alpha
    Environment :: GPU
    Intended Audience :: Developers
    Intended Audience :: Education
    Intended Audience :: Science/Research
    License :: OSI Approved :: BSD License
    Programming Language :: Python :: 3
    Programming Language :: Python :: 3.9
    Operating System :: OS Independent
    Topic :: Education
    Topic :: Scientific/Engineering :: Artificial Intelligence

[options]
package_dir =
    = src
packages = find:
python_requires = >=3.9
include_package_data = True
install_requires =
    beautifulsoup4 >= 4.9
    click >= 7.1
    numpy >= 1.20
    openpyxl >= 3.0
    pandas >= 1.2
    scikit-learn >= 0.24
    sentencepiece >= 0.1
    transformers >= 4.11
    xlrd >= 2.0

[options.extras_require]
dev =
    # Linting for code style
    flake8 >= 4.0
    # Build documentation
    numpydoc >= 1.1
    memory-profiler >= 0.58
    myst-parser >= 0.15
    Sphinx >= 4.3
    sphinxcontrib-bibtex >= 2.4
    sphinx-gallery >= 0.10
    sphinx-rtd-theme >= 1.0
    sphinx-copybutton >= 0.4
    # Run tests and publish coverage stats
    pytest >= 6.2
    coverage >= 6.1
    # Build distribution archive
    build >= 0.7
    # Check and upload distribution archives to PyPI
    twine >= 3.6
    # Hyperparameter tunning
    optuna >= 2.10
    # Run Jupyter Notebooks
    jupyterlab >= 3.2

[options.packages.find]
where = src

[flake8]
doctests = True
exclude = docs

[coverage:run]
branch = True
source = src/nlp_job_ads
