import torch

import numpy as np
import pandas as pd

from nlp_job_ads import utils
from sklearn.preprocessing import label_binarize
from transformers.tokenization_utils_base import BatchEncoding
from typing import Dict, List


def load_x28(path: str) -> pd.DataFrame:
    """Load x28 data from ``.json`` file

    Parameters
    ----------
    path : str
        Path to a ``.json`` file obtained from the x28 API

    Returns
    -------
    :class:`~pandas.DataFrame`
        A DataFrame with one row for each job ad in the ``.json`` file.
    """
    # Read JSON
    job_ads = pd.read_json(path)

    # Reference job ads by their unique identifier
    job_ads = job_ads.set_index('id')
    job_ads = job_ads.sort_index()

    return job_ads


def load_x28_annotated_edu(path: str) -> pd.DataFrame:
    r"""Parse x28 annotated education data to use in fine-tuning of classifier

    Parameters
    ----------
    path : string
        Path to the ``.xlsx`` annotated data file

    Returns
    -------
    :class:`~pandas.DataFrame`
        A table with ``str`` content and the following columns:

        - ``'id'``: the unique identifiers of the job ad in the x28 database,
        - ``'title'``: the title of the job ad,
        - ``'content'``: the text content of the job ad,
        - ``'edu_1'``: lowest education levels identified by the annotators,
        - ``'edu_2'``: second highest/lowest education levels,
        - ``'edu_3'``: highest education level.

    See also
    --------
    TODO: link to txt file explaining contents of the annotated data
    """

    # Load the .xlsx file into a pandas DataFrame. Retain only ID, title,
    # ad text, and education labels
    annotated_data = pd.read_excel(
        path,
        usecols=[
            "X28-ID",
            "X28-title",
            "Ad text",
            "1st educational requirement from list of labels ",
            "2nd educational requirement from list of labels ",
            "3rd educational requirement from list of labels "
            ]
        )

    # Shorten column names to ease usage
    annotated_data.rename(
        columns={
            "X28-ID": "id",
            "X28-title": "title",
            "Ad text": "content",
            "1st educational requirement from list of labels ": "edu_1",
            "2nd educational requirement from list of labels ": "edu_2",
            "3rd educational requirement from list of labels ": "edu_3"
        },
        inplace=True
        )

    # Set table index to ID column
    annotated_data = annotated_data.set_index('id')

    return annotated_data


def merge_title_and_content(title: pd.Series, content: pd.Series) -> pd.Series:
    """Merge title and content

    Merge the two pandas.Series into a single :class:`~pandas.Series`, while
    handling missing values and simplifying the joined texts

    Parameters
    ----------
    title : :class:`~pandas.Series`
        Title strings
    content : :class:`~pandas.Series`
        Content text strings

    Returns
    -------
    :class:`~pandas.Series`
        Merged and cleaned-up title and content strings
    """
    # Extract title and text columns
    title_and_content = pd.merge(title,
                                 content,
                                 right_index=True,
                                 left_index=True)

    # Replace NaN content with empty strings
    title_and_content.fillna(value='', inplace=True)

    # Merge title and content columns
    texts = title_and_content.agg(" ".join, axis=1)

    # Remove HTML markup
    texts = texts.apply(utils.remove_html_markup)

    # Simplify whitespaces on texts
    texts = texts.apply(utils.simplify_whitespace)

    return texts


def load_ft_extract(path: str) -> pd.DataFrame:
    """Load a previously saved fine-tunning data extract

    Parameters
    ----------
    path : str
        Path to the saved ``.csv`` file. It must represent a table with at
        least two columns, with names ``'id'`` and ``'labels'``

    Returns
    -------
    :class:`~pandas.DataFrame`
        The table represented in the provided ``.csv`` file
    """
    extract = pd.read_csv(path, index_col='id', converters={'labels': eval})
    return extract


class TextDataset(object):
    """Base class for text datasets with accompanying metadata

    Parameters
    ----------
    path : str
        Path to the data file(s) to load
    """

    def __init__(self, path: str) -> None:
        self.path = path
        self.data_table = self._load_data_table()
        super().__init__()

    def _load_data_table(self) -> pd.DataFrame:
        """Load data table

        Reads the data file from the path provided, process it, and returns a
        data table

        Returns
        -------
        :class:`~pandas.DataFrame`
            Processed data table

        Raises
        ------
        NotImplementedError
            Must be implemented by the user
        """
        raise NotImplementedError

    def _texts_from_table(self) -> List:
        """Extract list of texts from data table

        May assume that data table has been loaded and is accessible in
        :attr:`data_table`. Processes entries in the data table and return a
        list of strings containing what are desired to be seen as the texts in
        the dataset

        Returns
        -------
        List
            Texts

        Raises
        ------
        NotImplementedError
            Must be implemented by the user
        """
        raise NotImplementedError

    def get_texts(self) -> List:
        """Get list of texts in the dataset

        Returns
        -------
        List
            Texts
        """
        return self._texts_from_table()


class X28(TextDataset):
    """x28 Dataset class"""
    def _load_data_table(self):
        return load_x28(self.path)

    def _texts_from_table(self) -> List:
        texts = merge_title_and_content(self.data_table['title'],
                                        self.data_table['content'])
        return texts.to_list()

    def get_ids(self) -> List:
        """Get job ad IDs

        Returns
        -------
        List
            IDs of the job ads in the dataset, according to the x28 database
        """
        return self.data_table.index.to_list()


class AnnotatedEdu(TextDataset):
    """Dataset of x28 job ads annotated with education levels

    Parameters
    ----------
    path : str
        Path to the ``.xlsx`` annotated data file
    language : str, optional
        Language of the annotated texts, by default ``'de'``.
        See :func:`~nlp_job_ads.utils.get_edu_names` for options
    all_edu : bool, default=True
        Get multilabel binary vectors from all education fields in the
        annotated data. If False, only the lowest education level column is
        used to build the binary label vectors
    edu_redux : bool, default=True
        Used reduced set of education labels.
        See :func:`~nlp_job_ads.utils.edu2edu_redux` for the reduction mapping.
    """
    def __init__(self,
                 path: str,
                 language: str = "de",
                 all_edu: bool = True,
                 edu_redux: bool = False) -> None:
        """Build data table


        """
        self.language = language
        self.all_edu = all_edu
        self.edu_redux = edu_redux
        self.label_names = self._get_label_names()
        super().__init__(path)

    def _get_label_names(self) -> List:
        """Get label names

        Returns
        -------
        List
            Names of the labels, in order, represented in the entries of the
            binary label lists returned by :func:`~self.get_labels()`
        """
        if self.edu_redux:
            return utils.get_edu_redux_names(self.language)
        else:
            return utils.get_edu_names(self.language)

    def _load_data_table(self):
        return load_x28_annotated_edu(self.path)

    def _texts_from_table(self) -> List:
        texts = merge_title_and_content(self.data_table['title'],
                                        self.data_table['content'])
        return texts.to_list()

    def get_ids(self) -> List:
        """Get job ad IDs

        Returns
        -------
        List
            IDs of the job ads in the dataset, according to the x28 database
        """
        return self.data_table.index.to_list()

    def edu_redux_map(self, name):
        """Map education name from full set to reduced set"""
        dictionary = utils.edu2edu_redux(self.language)
        try:
            new_name = dictionary[name]
        except KeyError:
            new_name = name
        return new_name

    def get_labels(self) -> List[List[float]]:
        """Get binarized label vector

        Returns
        -------
        List[List[float]]
            Binary label lists for each job ad text. Each element of each label
            list represents a possible education level. A one in the label list
            indicates that the job ad has been annotated with the education
            level corresponding to the one's position. Zeros indicate lack of
            annotation. If :attr:`self.all_edu == True`, all education level
            fields are used to build the (multi)label lists.
        """
        # Binarize education labels using utils.get_edu_levels() as reference
        possible_labels = self.label_names
        edu_1 = self.data_table['edu_1'].fillna('').map(
            utils.simplify_whitespace
            )
        if self.edu_redux:
            # Map names to a reduced label set
            edu_1 = edu_1.map(self.edu_redux_map)
        bin_labels_edu_1 = label_binarize(edu_1,
                                          classes=possible_labels)

        if self.all_edu:
            # Compute binary encodings for the other education columns too
            edu_2 = self.data_table['edu_2'].fillna('').map(
                utils.simplify_whitespace
                )
            if self.edu_redux:
                # Map names to a reduced label set
                edu_2 = edu_2.map(self.edu_redux_map)
            bin_labels_edu_2 = label_binarize(edu_2, classes=possible_labels)

            edu_3 = self.data_table['edu_3'].fillna('').map(
                utils.simplify_whitespace
                )
            if self.edu_redux:
                # Map names to a reduced label set
                edu_3 = edu_3.map(self.edu_redux_map)
            bin_labels_edu_3 = label_binarize(edu_3, classes=possible_labels)

            # Merge three binary label vectors into one and make sure the
            # merge is still binary (i.e., fixing potential duplicate labels
            # across different education columns)
            labels = bin_labels_edu_1 + bin_labels_edu_2 + bin_labels_edu_3
            labels = np.clip(labels, a_min=0, a_max=1)

        else:
            labels = bin_labels_edu_1

        # Return binarized labels as a list of floats
        return labels.astype(float).tolist()

    def save_ft_extract(self, path: str):
        """Save extract of data table to be used in fine-tunning procedures

        Parameters
        ----------
        path : str
            Save path (with ``.csv`` extension) for the table extract. The
            saved table contains the following columns: ``'id'`` for the unique
            identifiers of the job ad in the x28 database, ``'text'`` for the
            merged and cleaned-up title and content of the job ads, and
            ``'label'`` for the binary (multi)label list identifying the
            education levels assigned to the job ad by the annotators

        Notes
        -----
        One should read the saved extract with a call like

        >>> import pandas as pd
        >>> path = 'path_to_extract.csv'
        >>> extract = pd.read_csv(path, converters={'labels': eval})

        so that the contents of the `labels` column are interpreted as lists,
        rather than strings. Alternatively, you may use the method
        :func:`~nlp_job_ads.dataset.load_ft_extract`

        See also
        --------
        :func:`~nlp_job_ads.dataset.load_ft_extract` : Load a saved
            fine-tunning extract
        """
        extract = pd.DataFrame(columns=['id', 'text', 'labels'])
        extract['id'] = self.get_ids()
        extract['text'] = self.get_texts()
        extract['labels'] = self.get_labels()
        extract.to_csv(path, index=False)


class SequenceClassification(torch.utils.data.Dataset):
    """PyTorch dataset used in fine-tunning a sequence classification task

    Parameters
    ----------
    encodings : BatchEncoding
        Encodings output by a :class:`~transformers.PreTrainedTokenizer`
    labels : List
        The labels corresponding to the encodings in the sequence
        classification task
    """
    def __init__(self, encodings: BatchEncoding, labels: List):
        self.encodings = encodings
        self.labels = labels

    def __getitem__(self, idx: int) -> Dict:
        """Get data item from index

        Parameters
        ----------
        idx : int
            Index

        Returns
        -------
        Dict
            Data item
        """
        item = {key: torch.tensor(val[idx])
                for key, val in self.encodings.items()}
        item['labels'] = torch.tensor(self.labels[idx])
        return item

    def __len__(self) -> int:
        """Get number of samples in the dataset

        Returns
        -------
        int
            Number of samples
        """
        return len(self.labels)
