r"""
The :mod:`nlp_job_ads` package is organized around the following modules:

- :mod:`.dataset` to load and prepare datasets,
- :mod:`.metrics` to evaluate prediction models,
- :mod:`.predict` to create prediction pipelines,
- :mod:`.utils` for miscellaneous utilities.

"""

__version__ = '1.0.0'
__author__ = 'Rodrigo C. G. Pena'
__credits__ = 'Center for Data Analysis (CeDA) - University of Basel'
