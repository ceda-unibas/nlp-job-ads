import os

import numpy as np

from nlp_job_ads import utils
from transformers import (
    AutoModelForSequenceClassification,
    AutoTokenizer,
    pipeline
)
from typing import Dict, List, Union


class ZeroShotSequenceClassifier(object):
    """Base Zero-shot Sequence Classifier class
    """
    def __init__(self, model_name: str, device: int = -1) -> None:
        """Build pipeline and hypothesis template

        Parameters
        ----------
        model_name : str
            Name of a the Natural Language Inference (NLI) model in the
            `huggingface` hub, or path to local model files
        device : int, optional
            Device ordinal for CPU/GPU supports. Setting this to -1 will
            leverage CPU, a positive integer will run the model on the
            associated CUDA device id. Default is -1
        """
        self.model_name = model_name
        self.device = device
        self.pipeline = pipeline("zero-shot-classification",
                                 self.model_name,
                                 device=self.device)
        self.hypothesis = self._get_hypothesis()
        self.label_names = self._get_label_names()
        super().__init__()

    def _get_hypothesis(self):
        """Get hypothesis template

        Must return a hypothesis string to be used for computing entailment
        probabilities with the Natural Language Inference (NLI) model

        Raises
        ------
        NotImplementedError
            Must be implemented by the user
        """
        raise NotImplementedError

    def _get_label_names(self):
        """Get label names

        Must return a list of strings containing the names of the labels in the
        classification task

        Raises
        ------
        NotImplementedError
            [description]
        """
        raise NotImplementedError

    def classify(self, sequences: List[str], **kwargs) -> List[Dict]:
        """Classify sequences into labels

        Parameters
        ----------
        sequences : List[str]
            The text sequences to classify
        **kwargs : dict
            Extra keyword arguments to be passed to
            :meth:`~transformers.pipeline`

        Returns
        -------
        List[Dict]
            Each element of the list is a dictionary representing an input
            sequence. Each key of the dictionary represents a label. Each value
            of the dictionary represents the classification score assigned by
            the model to te corresponding label key

        See also
        --------
        :meth:`~transformers.pipeline` : Utility factory method to build a
            :class:`~transformers.Pipeline`
        """
        results = self.pipeline(sequences,
                                candidate_labels=self.label_names,
                                hypothesis_template=self.hypothesis,
                                multi_label=True,
                                **kwargs)
        if isinstance(results, dict):
            results = [results]
        scores = []
        for result in results:
            rearranged_scores = utils.rearrange_scores(result['scores'],
                                                       result['labels'],
                                                       self.label_names)
            scores.append(dict(zip(self.label_names, rearranged_scores)))
        return scores


class ZeroShotEdu(ZeroShotSequenceClassifier):
    """Zero-shot sequence classifier adapted to education labels
    """
    def __init__(self, language: str, *args, **kwargs) -> None:
        """
        Parameters
        ----------
        model_name : str
            Name of a the Natural Language Inference (NLI) model in the
            `huggingface` hub, or path to local model files
        language : str
            Language in which to write the hypothesis template for the
            zero-shot classification pipeline
        """
        self.language = language
        super().__init__(*args, **kwargs)

    def _get_hypothesis(self):
        return utils.get_edu_hypothesis(self.language)

    def _get_label_names(self):
        return utils.get_edu_names(self.language)


class ZeroShotEduRedux(ZeroShotSequenceClassifier):
    """Zero-shot sequence classifier adapted to reduced education labels
    """
    def __init__(self, language: str, *args, **kwargs) -> None:
        """
        Parameters
        ----------
        model_name : str
            Name of a the Natural Language Inference (NLI) model in the
            `huggingface` hub, or path to local model files
        language : str
            Language in which to write the hypothesis template for the
            zero-shot classification pipeline
        """
        self.language = language
        super().__init__(*args, **kwargs)

    def _get_hypothesis(self):
        return utils.get_edu_hypothesis(self.language)

    def _get_label_names(self):
        return utils.get_edu_redux_names(self.language)


class SequenceClassifier(object):
    """Base Sequence Classification class
    """
    def __init__(self, model_name: str, device: int = -1) -> None:
        """Build the pipeline

        Parameters
        ----------
        model_name : str
            Name of a sequence classification model in the
            `huggingface` hub, or path to local model files
        device : int, optional
            Device ordinal for CPU/GPU supports. Setting this to -1 will
            leverage CPU, a positive integer will run the model on the
            associated CUDA device id. Default is -1
        """
        self.model_name = model_name
        self.device = device
        self.model = AutoModelForSequenceClassification.from_pretrained(
            self.model_name,
            id2label=self._get_id2label()
            )
        self.tokenizer = AutoTokenizer.from_pretrained(self.model_name)
        self.pipeline = pipeline("text-classification",
                                 model=self.model,
                                 tokenizer=self.tokenizer,
                                 device=self.device,
                                 padding=True,
                                 truncation=True)
        self.label_names = self._get_label_names()
        super().__init__()

    def _get_id2label(self):
        """Build id2label dictionary from list of label names

        Returns
        -------
        Dict
            Keys are integers from `0` to `len(self.label_names) - 1`, values
            are the label names
        """
        values = self._get_label_names()
        keys = np.arange(len(values)).tolist()
        return dict(zip(keys, values))

    def _get_label_names(self):
        """Get label names

        Must return a list of strings containing the names of the labels for
        the classification task

        Raises
        ------
        NotImplementedError
            Must be implemented by the user
        """
        raise NotImplementedError

    def classify(self,
                 sequences: Union[str, List[str]],
                 **kwargs) -> List[Dict]:
        """Classify sequences into labels

        Parameters
        ----------
        sequences : List[str]
            The text sequences to classify
        **kwargs : dict
            Extra keyword arguments to be passed to
            :meth:`~transformers.pipeline`

        Returns
        -------
        List[Dict]
            Each element of the list is a dictionary representing an input
            sequence. Each key of the dictionary represents a label. Each value
            of the dictionary represents the classification score assigned by
            the model to te corresponding label key

        See also
        --------
        :func:`~transformers.pipeline` : Utility factory method to build a
            :class:`~transformers.Pipeline`
        """
        results = self.pipeline(sequences, top_k=None, **kwargs)
        scores = []
        for result in results:
            sequence_labels = [d['label'] for d in result]
            sequence_scores = [d['score'] for d in result]
            rearranged_scores = utils.rearrange_scores(sequence_scores,
                                                       sequence_labels,
                                                       self.label_names)
            scores.append(dict(zip(self.label_names, rearranged_scores)))
        return scores


class EduClassifier(SequenceClassifier):
    """Sequence classifier with label names taken from the education
    requirements set

    Parameters
    ----------
    language : str
        Language of the labels

    See also
    --------
    :func:`~nlp_job_ads.utils.get_edu_names` : Get list of education names
    """
    def __init__(self, language: str, *args, **kwargs) -> None:
        self.language = language
        super().__init__(*args, **kwargs)

    def _get_label_names(self):
        return utils.get_edu_names(self.language)


class EduReduxClassifier(SequenceClassifier):
    """Sequence classifier with label names taken from the reduced education
    requirements set

    Parameters
    ----------
    language : str
        Language of the labels

    See also
    --------
    :func:`~nlp_job_ads.utils.get_edu_redux_names` : Get reduced list of
        education names
    """
    def __init__(self, language: str, *args, **kwargs) -> None:
        """_summary_

        Parameters
        ----------
        language : str
            _description_
        """
        self.language = language
        super().__init__(*args, **kwargs)

    def _get_label_names(self):
        return utils.get_edu_redux_names(self.language)


class GBERTBaseEdu(EduClassifier):
    """GBERT-base fine-tuned for the education classification task
    """
    def __init__(self, **kwargs) -> None:
        # TODO: Try to download model from huggingface hub when possible
        home = os.environ['HOME']
        path_or_url = "models/nlp-job-ads/gbert-base-ft-edu/"
        path_or_url = os.path.join(home, path_or_url)
        super().__init__(model_name=path_or_url, language="de", **kwargs)


class GBERTBaseEduRedux(EduReduxClassifier):
    """GBERT-base fine-tuned for the education classification task (redux)
    """
    def __init__(self, **kwargs) -> None:
        path_or_url = "gonzpen/gbert-base-ft-edu-redux"
        super().__init__(model_name=path_or_url, language="de", **kwargs)


class GBERTLargeEduRedux(EduReduxClassifier):
    """GBERT-large fine-tuned for the education classification task (redux)
    """
    def __init__(self, **kwargs) -> None:
        path_or_url = "gonzpen/gbert-large-ft-edu-redux"
        super().__init__(model_name=path_or_url, language="de", **kwargs)
