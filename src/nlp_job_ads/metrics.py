import torch

import numpy as np

from nlp_job_ads import utils
from sklearn.metrics import (
    accuracy_score,
    average_precision_score,
    f1_score,
    hamming_loss,
    label_ranking_average_precision_score,
    label_ranking_loss,
    ndcg_score,
    roc_auc_score,
    zero_one_loss
)
from torch import Tensor
from transformers.trainer_utils import EvalPrediction
from typing import Dict, Optional, Union


def _best_threshold(y_true,
                    y_probs,
                    score_fun,
                    t_min=None,
                    t_max=None,
                    n_points=None,
                    sample_weight=None):
    """Grid-search the best threshold for a scoring function

    Parameters
    ----------
    y_true : array-like
        Reference values (labels) for the scoring function
    y_probs : array-like
        Probability-like values coming from an estimator for the reference
        labels
    score_fun : function
        Should take the two array-like objects, `y_true` and `y_probs` and
        return a floating point number
    t_min : float, optional
        Lower-limit for the threshold search, by default 0.1
    t_max : float, optional
        Higher-limit for the threshold search, by default 0.9
    n_points : int, optional
        Number of points to search in the grid, by default 100
    sample_weight : array-like of length ``len(y_true)``, optional
        Sample weights, by default None

    Returns
    -------
    tuple
        The best threshold and its corresponding score
    """
    # Set default grid-search parameters
    t_min = 0.1 if t_min is None else t_min
    t_max = 0.9 if t_max is None else t_max
    n_points = 100 if n_points is None else n_points

    # Search
    best_thresh = None
    best_score = -np.inf
    for thresh in np.linspace(t_min, t_max, num=n_points):
        predictions = (y_probs > thresh).astype(float)
        score = score_fun(y_true, predictions, sample_weight=sample_weight)
        if score > best_score:
            best_thresh = thresh
            best_score = score

    return best_thresh, best_score


def _scorer_call(eval_pred: EvalPrediction,
                 scorer_name: str,
                 scorer_fun: callable,
                 thresholding: bool = False,
                 thresh: Optional[float] = None,
                 weigh_samples: bool = False) -> Dict:
    """The Scorer call method

    Parameters
    ----------
    eval_pred : EvalPrediction
        A set of logits and reference labels to compare (evaluate).
        The labels should be binary lists, with one element for each
        represented class
    scorer_name : str
        Name of the score function
    scorer_fun : callable
        Scoring function that compares reference with estimated values
    thresholding : bool, optional
        Threshold the classifier probabilites before feeding them to the
        scoring function (True) or not (False), by default False
    thresh : Optional[float], optional
        The thresholding value to use if `thresholding==True`, by default None,
        which means a good threshold will be looked for via grid search
    weigh_samples : bool, optional
        Weigh samples according to class frequencies, by default False

    Returns
    -------
    Dict
        A single-element dictionary with key `scorer_name` and a
        corresponding value `score` computed from `scorer_fun`
    """
    # Get logits and labels from input
    logits, labels = eval_pred

    # Softmax the logits to get probability-like values
    probs = np.array(torch.nn.Softmax(dim=-1)(torch.Tensor(logits)))

    # Get sample weights
    if weigh_samples:
        sample_weight = utils.get_sample_weight(labels)
    else:
        sample_weight = None

    if thresholding:
        # Binarize the probabilities with a (possibly optimal) threshold

        # Set grid search parameters
        if thresh is None:
            # Default parameters
            t_min = None
            t_max = None
            n_points = None

        else:
            # Tweak parameters so that grid has only one point and it is equal
            # to `thresh`
            t_min = thresh
            t_max = thresh
            n_points = 1

        # Get score
        _, score = _best_threshold(y_true=labels,
                                   y_probs=probs,
                                   score_fun=scorer_fun,
                                   t_min=t_min,
                                   t_max=t_max,
                                   n_points=n_points,
                                   sample_weight=sample_weight)
    else:
        # Get score
        score = scorer_fun(y_true=labels,
                           y_pred=probs,
                           sample_weight=sample_weight)

    return {scorer_name: score}


class Scorer(object):
    """Base scoring class"""

    def __init__(self,
                 thresholding: bool = False,
                 thresh: Optional[float] = None,
                 weigh_samples: bool = False) -> None:
        self.thresholding = thresholding
        self.thresh = thresh
        self.weigh_samples = weigh_samples

    def __call__(self, eval_pred: EvalPrediction) -> Dict:
        result = _scorer_call(eval_pred,
                              scorer_name=self._get_name(),
                              scorer_fun=self._get_score,
                              thresholding=self.thresholding,
                              thresh=self.thresh,
                              weigh_samples=self.weigh_samples)
        return result

    def _get_score(y_true: np.array, y_pred: Tensor) -> float:
        """Score from comparing reference values with estimated probabilities

        Parameters
        ----------
        y_true : np.array
            Reference values (labels)
        y_pred : Tensor
            Probabilities from an estimator

        Returns
        -------
        float
            Score

        Raises
        ------
        NotImplementedError
            Must be implemented by the user
        """
        raise NotImplementedError

    def _get_name() -> str:
        """Get score function name

        Returns
        -------
        str
            Name of the score function

        Raises
        ------
        NotImplementedError
            Must be implemented by the user
        """
        raise NotImplementedError


class Accuracy(Scorer):
    """Accuracy Scorer

    In multilabel classification, this scorer computes subset accuracy: the set
    of labels predicted for a sample must exactly match the corresponding set
    of labels in `y_true`
    """
    def __init__(self, **kwargs) -> None:
        kwargs.pop('thresholding', None)
        super().__init__(thresholding=True, **kwargs)

    def _get_name(self) -> str:
        return 'accuracy_score'

    def _get_score(self,
                   y_true: np.array,
                   y_pred: Tensor,
                   sample_weight=None) -> float:
        score = accuracy_score(y_true=y_true,
                               y_pred=y_pred,
                               sample_weight=sample_weight)
        return score


class AvgPrecision(Scorer):
    """Average Precision Scorer

    Average Precision summarizes a precision-recall curve as the weighted mean
    of precisions achieved at each threshold, with the increase in recall from
    the previous threshold used as the weight

    Parameters
    ----------
    average : {``'micro'``,``'macro'``,``'samples'``,``'weighted'``,``None``} ,
    optional
        How to aggregate the AUC scores across classes, by default 'weighted'.
        See `sklearn.metrics.average_precision_score`.
    """
    def __init__(self, average: Optional[str] = 'weighted', **kwargs) -> None:
        self.average = average
        kwargs.pop('thresholding', None)
        super().__init__(thresholding=False, **kwargs)

    def _get_name(self) -> str:
        if self.average is None:
            name = "avg_precision_per_class"
        else:
            name = "avg_precision_{}".format(self.average)

        return name

    def _get_score(self,
                   y_true: np.array,
                   y_pred: Tensor,
                   sample_weight=None) -> float:
        score = average_precision_score(y_true=y_true,
                                        y_score=y_pred,
                                        average=self.average,
                                        sample_weight=sample_weight)
        return score


class AUC(Scorer):
    """Area Under the ROC Curve Scorer

    This scorer computes the Area Under the Receiver Operating Characteristic
    Curve (ROC AUC) from prediction scores.

    Parameters
    ----------
    average : {``'micro'``, ``'macro'``, ``'samples'``, ``'weighted'``} or
        None, optional
        How to aggregate the AUC scores across classes, by default 'weighted'.
        See `sklearn.metrics.roc_auc_score`.

    Notes
    -----
    This scorer raises 'ERROR: Only one class present in y_true. ROC AUC score
    is not defined in that case.' if there are too few examples of some labels
    in the reference set.
    """
    def __init__(self, average: Optional[str] = 'weighted', **kwargs) -> None:
        self.average = average
        kwargs.pop('thresholding', None)
        super().__init__(thresholding=False, **kwargs)

    def _get_name(self) -> str:
        if self.average is None:
            name = "auc_per_class"
        else:
            name = "auc_{}".format(self.average)

        return name

    def _get_score(self,
                   y_true: np.array,
                   y_pred: Tensor,
                   sample_weight=None) -> float:
        score = roc_auc_score(y_true=y_true,
                              y_score=y_pred,
                              average=self.average,
                              sample_weight=sample_weight)
        return score


class F1(Scorer):
    """F1 Scorer

    The F1 score can be interpreted as a harmonic mean of the precision and
    recall, where an F1 score reaches its best value at 1 and worst score at 0.
    The relative contribution of precision and recall to the F1 score are
    equal.

    In the multi-class and multi-label case, this is the average of the F1
    score of each class with weighting depending on the `average` parameter.

    Parameters
    ----------
    average : {``'micro'``,``'macro'``,``'samples'``,``'weighted'``,
    ``'binary'``} or ``None``,
              optional
        How to aggregate the F1 scores across classes, by default 'weighted'.
        See `sklearn.metrics.f1_score`.
    """
    def __init__(self,
                 thresh: Optional[float] = None,
                 average: Union[str, None] = 'weighted',
                 **kwargs) -> None:
        self.average = average
        kwargs.pop('thresholding', None)
        kwargs.pop('thresh', None)
        super().__init__(thresholding=True, thresh=thresh, **kwargs)

    def _get_name(self) -> str:
        if self.average is None:
            name = "f1_per_class"
        else:
            name = "f1_{}".format(self.average)

        return name

    def _get_score(self,
                   y_true: np.array,
                   y_pred: Tensor,
                   sample_weight=None) -> float:
        score = f1_score(y_true=y_true,
                         y_pred=y_pred,
                         average=self.average,
                         zero_division=0,
                         sample_weight=sample_weight)
        return score


class Hamming(Scorer):
    """Hamming Loss Scorer

    The Hamming loss is the fraction of labels that are incorrectly predicted.

    In multilabel classification, the Hamming loss is different from the subset
    zero-one loss. The zero-one loss considers the entire set of labels for a
    given sample incorrect if it does not entirely match the true set of
    labels. Hamming loss is more forgiving in that it penalizes only the
    individual labels.

    The Hamming loss is upperbounded by the subset zero-one loss, when
    normalize parameter is set to True. It is always between 0 and 1, lower
    being better.
    """
    def __init__(self, thresh: Optional[float] = None, **kwargs) -> None:
        kwargs.pop('thresholding', None)
        kwargs.pop('thresh', None)
        super().__init__(thresholding=True, thresh=thresh, **kwargs)

    def _get_name(self) -> str:
        return 'hamming_loss'

    def _get_score(self,
                   y_true: np.array,
                   y_pred: Tensor,
                   sample_weight=None) -> float:
        score = hamming_loss(y_true=y_true,
                             y_pred=y_pred,
                             sample_weight=sample_weight)
        return score


class LRAP(Scorer):
    """Label Raking Average Precision Scorer

    Averages over the samples the answer to the following question: for each
    ground truth label, what fraction of higher-ranked labels were true labels?
    This performance measure will be higher if you are able to give better rank
    to the labels associated with each sample. The obtained score is always
    strictly greater than 0, and the best value is 1.
    """
    def __init__(self, **kwargs) -> None:
        kwargs.pop('thresholding', None)
        super().__init__(thresholding=False, **kwargs)

    def _get_name(self) -> str:
        return 'lrap'

    def _get_score(self,
                   y_true: np.array,
                   y_pred: Tensor,
                   sample_weight=None) -> float:
        score = label_ranking_average_precision_score(
            y_true=y_true,
            y_score=y_pred,
            sample_weight=sample_weight
            )
        return score


class LabelRanking(Scorer):
    """Label Raking Loss Scorer

    Computes a loss which averages over the samples the number of label pairs
    that are incorrectly ordered, weighted by the inverse of the number of
    ordered pairs of false and true labels. The lowest achievable ranking loss
    is zero.
    """
    def __init__(self, **kwargs) -> None:
        kwargs.pop('thresholding', None)
        super().__init__(thresholding=False, **kwargs)

    def _get_name(self) -> str:
        return 'label_ranking_loss'

    def _get_score(self,
                   y_true: np.array,
                   y_pred: Tensor,
                   sample_weight=None) -> float:
        score = label_ranking_loss(y_true=y_true,
                                   y_score=y_pred,
                                   sample_weight=sample_weight)
        return score


class NDCG(Scorer):
    """Normalized Discounted Cumulative Gain Scorer

    Scorer that sums the true scores ranked in the order induced by the
    predicted scores, after applying a logarithmic discount. Then divide by the
    best possible score to obtain a score between 0 and 1.

    Compared with the ranking loss, NDCG can take into account relevance
    scores, rather than a ground-truth ranking. So if the ground-truth consists
    only of an ordering, the ranking loss should be preferred; if the
    ground-truth consists of actual usefulness scores (e.g. 0 for irrelevant, 1
    for relevant, 2 for very relevant), NDCG can be used.

    Parameters
    ----------
    k : int, optional
        Only consider the highest k scores in the ranking, by default None. If
        None, use all outputs.
    """
    def __init__(self, k: int = None, **kwargs) -> None:
        self.k = k
        kwargs.pop('thresholding', None)
        super().__init__(thresholding=False, **kwargs)

    def _get_name(self) -> str:
        name = 'ndcg'
        if self.k is not None:
            name += '_{}'.format(self.k)
        return name

    def _get_score(self,
                   y_true: np.array,
                   y_pred: Tensor,
                   sample_weight=None) -> float:
        score = ndcg_score(y_true=y_true,
                           y_score=y_pred,
                           k=self.k,
                           sample_weight=sample_weight)
        return score


class ZeroOne(Scorer):
    """Zero-One Loss Scorer

    In multilabel classification, the zero_one_loss function corresponds to the
    subset zero-one loss: for each sample, the entire set of labels must be
    correctly predicted, otherwise the loss for that sample is equal to one.

    Parameters
    ----------
    normalize : bool, optional
        Compute fraction of misclassifications (True) or the number of
        misclassifications (False), default is True.
        See `sklearn.metrics.zero_one_loss`.
    """
    def __init__(self,
                 thresh: Optional[float] = None,
                 normalize: Optional[bool] = True,
                 **kwargs) -> None:
        self.normalize = normalize
        kwargs.pop('thresholding', None)
        kwargs.pop('thresh', None)
        super().__init__(thresholding=True, thresh=thresh, **kwargs)

    def _get_name(self) -> str:
        return 'zero_one_loss'

    def _get_score(self,
                   y_true: np.array,
                   y_pred: Tensor,
                   sample_weight=None) -> float:
        score = zero_one_loss(y_true=y_true,
                              y_pred=y_pred,
                              normalize=self.normalize,
                              sample_weight=sample_weight)
        return score


def get_scorer(name: str, weigh_samples: bool = False) -> Scorer:
    """Get scorer instance from name

    Parameters
    ----------
    name : str
        Code name for the scorer. Options are {'accuracy', 'auc', 'auc_micro',
        'auc_macro', 'avg-precision', 'avg-precision_micro',
        'avg-precision_macro', 'avg-precision_samples',
        'avg-precision_weighted', 'auc_samples', 'auc_weighted', 'f1',
        'f1_micro', 'f1_macro', 'f1_samples', 'f1_weighted', 'f1_binary',
        'hamming-loss', 'label-ranking-loss', 'lrap', 'ndcg_1', 'ndcg_2', ...,
        'zero-one-loss'}
    weigh_samples : bool, optional
        Weigh samples according to class frequencies, by default False

    Returns
    -------
    Scorer
        A scorer instance

    Raises
    ------
    ValueError
        If `name` is not a string
    ValueError
        If `name` does not correspond to an available scorer
    """
    if isinstance(name, str):
        substrings = name.split('_')
    else:
        raise ValueError("Name must be a string")

    kwargs = {'weigh_samples': weigh_samples}
    head = substrings[0]
    if head == 'accuracy':
        scorer = Accuracy(**kwargs)
    elif head == 'auc':
        kwargs['average'] = None if len(substrings) == 1 else substrings[1]
        scorer = AUC(**kwargs)
    elif head == 'avg-precision':
        kwargs['average'] = None if len(substrings) == 1 else substrings[1]
        scorer = AvgPrecision(**kwargs)
    elif head == 'f1':
        kwargs['average'] = None if len(substrings) == 1 else substrings[1]
        scorer = F1(**kwargs)
    elif head == 'hamming-loss':
        scorer = Hamming(**kwargs)
    elif head == 'label-ranking-loss':
        scorer = LabelRanking(**kwargs)
    elif head == 'lrap':
        scorer = LRAP(**kwargs)
    elif head == 'ndcg':
        kwargs['k'] = None if len(substrings) == 1 else int(substrings[1])
        scorer = NDCG(**kwargs)
    elif head == 'zero-one-loss':
        scorer = ZeroOne(**kwargs)
    else:
        raise ValueError("Name does not correspond to an available scorer")

    return scorer
