import datetime
import os

import numpy as np

from bs4 import BeautifulSoup
from sklearn.utils.class_weight import compute_sample_weight
from typing import List, Union


def remove_html_markup(html_text, parser="html.parser"):
    """Remove HTML markup from text

    Parameters
    ----------
    htmltxt : str or an open filehandle
        Text with HTML markup
    parser : str
        HTML parser to use, by default "html.parser"

    Returns
    -------
    str
        Text without HTML markup
    """
    if isinstance(html_text, str):
        soup = BeautifulSoup(html_text, parser)
        return soup.text
    else:
        return html_text


def simplify_whitespace(string):
    """Replace all whitespace characters in a string by simple whitespaces

    Parameters
    ----------
    string : str
        Input string

    Returns
    -------
    string
        The parsed string
    """
    if isinstance(string, str):
        # Replace double whitespace conventions from pandas into conventions
        # understandable by the Python standard library
        string = string.replace("\\r", '\r\r')
        string = string.replace("\\n", '\n\n')

        # Split sentence at all whitespace characters and join it again using
        # simple spaces
        string = " ".join(string.split())

    return string


def timestamp_to_date_string(timestamp):
    """Date string from time stamp

    Parameters
    ----------
    timestamp : int
        ISO-8601 compliant timestamp

    Returns
    -------
    str
        A date string in the format 'YYYY-MM-DD|hh:mm:ss'
    """
    date = datetime.datetime.fromtimestamp(timestamp / 1000)
    return date.isoformat('|', "seconds")


def get_edu_hypothesis(language: str):
    """Education hypothesis to be used in zero-shot classification

    Parameters
    ----------
    language : str
        Language of the hypothesis. Options are: 'en' (English), 'de' (German),
        'fr' (French), 'it' (Italian)

    Returns
    -------
    str
        The hypothesis string

    Raises
    ------
    ValueError
        If `language` does not correspond one of the available languages
    """
    if language == 'en':
        hypothesis = "The education required for this job is {}."
    elif language == 'de':
        hypothesis = "Die für diesen Job erforderliche Ausbildung ist {}."
    elif language == 'fr':
        hypothesis = "La formation requise pour cet emploi est {}."
    elif language == 'it':
        hypothesis = "L'istruzione richiesta per questo lavoro è {}."
    else:
        raise ValueError("Language options are 'en', 'de', 'fr', or 'it'.")
    return hypothesis


def get_edu_names(language):
    """Get list of education names in the specified language

    Parameters
    ----------
    language : str
        Acronym for the language. Options are {'de', 'fr', 'it'},
        for German, French, and Italian, respectively.

    Returns
    -------
    list of str
        List of education names in the specified language. If the
        input language string is not among the available options, then this
        function returns an empty list.
    """
    if language == 'de':
        return [
            'keine Angabe',
            'keine Ausbildungserfordernisse',
            'OS',
            'Matura',
            'Berufsausbildung',
            'Höhere Berufsausbildung',
            'FH-Bachelor',
            'UH-Bachelor',
            'FH-Master',
            'UH-Master',
            'Berufsbegleitende Nachdiplomausbildung',
            'Doktorat oder äquivalent',
            'EDK-Lehrdiplom + PH-Bachelor',
            'EDK-Diplom Logopädie + PH-Bachelor',
            'EDK-Diplom Sonderpädagogik + PH-Master',
            'EDK-Lehrdiplom + PH-Master und Äquivalente',
            'EDK-Lehrdiplom/Upper Secondary School Teaching Diploma ' +
            '+ FH- und/oder UH-Master'
                ]
    if language == 'fr':
        return []
    if language == 'it':
        return []
    else:
        return []


def is_edu_name(edu_string, language):
    """Check if input string represents an educational level

    Parameters
    ----------
    edu_string : str
        Educational level string
    language : str
        Language of the educational level string.
        Options are {'de', 'fr', 'it'}, for German, French, and Italian,
        respectively.

    Returns
    -------
    bool
        True if the query string was found among the possible educational level
        strings for the input language. False otherwise.
    """
    return any([edu_string == level for level in get_edu_names(language)])


def id2edu(language):
    """Get dictionary to convert integer IDs to education labels

    Parameters
    ----------
    language : str
        Acronym for the language. Options are {'de', 'fr', 'it'},
        for German, French, and Italian, respectively.

    Returns
    -------
    dict
        Dictionary for converting integer IDs to education labels in the
        specified language.
    """
    values = get_edu_names(language)
    keys = np.arange(len(values)).tolist()
    return dict(zip(keys, values))


def edu2id(language, redux=False):
    """Get dictionary to convert education labels to integer IDs

    Parameters
    ----------
    language : str
        Acronym for the language. Options are {'de', 'fr', 'it'},
        for German, French, and Italian, respectively.
    redux : bool, optional
        Use reduced education label set, by default False.

    Returns
    -------
    dict
        Dictionary for converting integer IDs to education labels in the
        specified language.
    """
    if redux:
        keys = get_edu_redux_names(language)
    else:
        keys = get_edu_names(language)
    values = np.arange(len(keys)).tolist()
    return dict(zip(keys, values))


def edu2edu_redux(language):
    """Get dictionary to convert education labels to a reduced label set

    Parameters
    ----------
    language : str
        Acronym for the language. Options are {'de', 'fr', 'it'},
        for German, French, and Italian, respectively.

    Returns
    -------
    dict
        Dictionary for converting labels in the original set to labels in the
        reduced set.
    """
    keys = get_edu_names(language)
    if language == "de":
        values = [
            'Sonstiges',
            'keine Ausbildungserfordernisse',
            'Sonstiges',
            'Sonstiges',
            'Berufsausbildung',
            'Höhere Berufsausbildung',
            'Bachelor',
            'Bachelor',
            'Master',
            'Master',
            'Bachelor',
            'Doktorat oder äquivalent',
            'Bachelor',
            'Bachelor',
            'Master',
            'Master',
            'Master',
        ]
    else:
        values = keys
    return dict(zip(keys, values))


def get_edu_redux_names(language):
    """Get reduced list of education names in the specified language

    Parameters
    ----------
    language : str
        Acronym for the language. Options are {'de', 'fr', 'it'},
        for German, French, and Italian, respectively.

    Returns
    -------
    list of str
        Reduced list of education names in the specified language. If the
        input language string is not among the available options, then this
        function returns an empty list.
    """
    return list(np.unique(list(edu2edu_redux(language).values())))


def rearrange_scores(scores: List,
                     current_label_order: List,
                     desired_label_order: List) -> List:
    """Rearrange label scores according to some reference label order

    Parameters
    ----------
    scores : List
        The scores
    current_label_order : List
        The current label order, that is `scores[0]` refers to
        `current_label_order[0]`, `scores[1]` to `current_label_order[1]`,
        and so on
    desired_label_order : List
        The desired label order. Must have the same set of elements as
        `current_label_order`

    Returns
    -------
    List
        A rearranged version, `rearranged_scores`, of `scores`, such that
        `rearranged_scores[0]` refers to `desired_label_order[0]`,
        `rearranged_scores[1]` to `desired_label_order[1]`, and so on
    """
    label2id = dict(zip(current_label_order,
                        np.arange(len(current_label_order))))
    rearranged_scores = [
        scores[label2id[label]] for label in desired_label_order
        ]
    return rearranged_scores


def get_file_names_with_ext(path: str, ext: str) -> List[str]:
    """List all files with the specified extension in the specified path

    Parameters
    ----------
    path : str
        Directory to search
    ext : str
        Extension of interest

    Returns
    -------
    List[str]
        List of file names (with extension)
    """
    file_names = [f for f in os.listdir(path) if f.endswith(ext)]
    return file_names


def get_counts_from_ml_vects(
    ml_vects: Union[List[List], List[np.array], np.array],
    label_names: List[str]
        ) -> dict:
    """Get class counts from multilabel vectors

    Parameters
    ----------
    ml_vects : Union[List[List], List[numpy.array], numpy.array]
        List of multilabel vectors. If a :class:`~numpy.array`, then the
        multilabel vectors should be the rows
    label_names : List[str]
        List of label names, with one name per element of the multilabel
        vectors

    Returns
    -------
    dict
        Each key is a label name, each value is the label's corresponding count
        in the provided list of multilabel vectors

    Examples
    --------
    >>> from nlp_job_ads.utils import get_counts_from_ml_vects
    >>> ml_vects = [[0, 1], [0, 1], [1, 0]]
    >>> label_names = ['cat', 'dog']
    >>> get_counts_from_ml_vects(ml_vects, label_names)
    {'cat': 1, 'dog': 2}
    """
    counts = np.array(ml_vects).sum(axis=0).astype(int)
    names_and_counts = dict(zip(label_names, counts))
    return names_and_counts


def get_sample_weight(y_true) -> np.array:
    """Get sample weights for a multilabel array based on the inverse of class
    frequencies

    Parameters
    ----------
    y_true : array-like of shape (n_samples, n_labels)
        Array of class labels per sample

    Returns
    -------
    np.array of shape (n_samples,)
        Sample weights
    """
    # Build sample weights from the inverse class-label frequencies
    sample_weight = compute_sample_weight('balanced', y_true)

    return sample_weight


def sample_from_label(texts, multi_labels, label_name):
    """Sample text associated with the specified label name

    Parameters
    ----------
    texts : array-like
        List of texts
    labels : array-like
        List of multi-label vectors associated with the texts
    label_name : str
        Label name defining the sampling set. Should be one of the values
        returned by :func:`~edu2id`

    Returns
    -------
    Tuple[str, list]
        A sampled text and its associated multilabel vector
    """
    # Turn inputs to arrays
    multi_labels = np.array(multi_labels)
    texts = np.array(texts)

    # Get the column index for the queried label name
    education_labels = edu2id(language="de")
    label_idx = education_labels[label_name]

    # Restrict dataset to texts and labels corresponding only to the queried
    # label name
    mask = (multi_labels[:, label_idx] == 1.0)
    if sum(mask) == 0:
        print("No matches found for this label name")
        return None, None
    else:
        matching_labels = multi_labels[mask, :]
        matching_texts = texts[mask]

    # Draw an entry at random from the restricted set
    row_number = np.random.choice(len(matching_texts))
    sampled_text = matching_texts[row_number]
    sampled_multi_label = matching_labels[row_number, :]

    return sampled_text, sampled_multi_label
