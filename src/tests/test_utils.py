from nlp_job_ads.utils import (
    remove_html_markup
    )


def test_remove_html_markup():
    html = "<h1>Hello World</h1>"
    txt = remove_html_markup(html_text=html)
    assert txt == "Hello World"
