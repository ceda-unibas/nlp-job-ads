#!/bin/bash

#Name of the job
#SBATCH --job-name=train

#Number of CPU cores reserved
#SBATCH --cpus-per-task=4

#Memory reserved per core.
#SBATCH --mem-per-cpu=8G

#GPU setup
#SBATCH --partition=pascal
#SBATCH --gres=gpu:4

#Time during which the task will run
#SBATCH --time=06:00:00
#SBATCH --qos=6hours

#Paths to STDOUT or STDERR files should be absolute or relative to the current
#working directory
#SBATCH --output=logs/train.o
#SBATCH --error=logs/train.e

#Notify via email when the task ends or fails
#SBATCH --mail-type=END,FAIL,TIME_LIMIT

#This job runs from the current working directory

#Load the required modules
##########################
ml cuDNN/7.6.4.38-gcccuda-2019b
eval "$(conda shell.bash hook)"
conda activate nlp-job-ads

#Run the desired commands
#########################
python scripts/training/train.py
