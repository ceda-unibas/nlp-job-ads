#!/bin/bash

#Name of the job
#SBATCH --job-name=test-gpu-availability

#Number of CPU cores reserved
#SBATCH --cpus-per-task=1

#Memory reserved per core.
#SBATCH --mem-per-cpu=1G

#GPU setup
#SBATCH --partition=rtx4090
#SBATCH --gres=gpu:2

#Time during which the task will run
#SBATCH --time=00:30:00
#SBATCH --qos=gpu30min

#Paths to STDOUT or STDERR files should be absolute or relative to the current
#working directory
#SBATCH --output=logs/test_gpu_availability.o
#SBATCH --error=logs/test_gpu_availability.e

#This job runs from the current working directory

#Load the required modules
##########################
ml cuDNN/7.6.4.38-gcccuda-2019b
eval "$(conda shell.bash hook)"
conda activate nlp-job-ads

#Export the required environment variables
##########################################

#Run the desired commands
#########################
python scripts/misc/test_gpu_availability.py
