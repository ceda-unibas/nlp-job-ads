#!/bin/bash

#Name of the job
#SBATCH --job-name=hp-search

#Number of CPU cores reserved
#SBATCH --cpus-per-task=4

#Memory reserved per core.
#SBATCH --mem-per-cpu=8G

#GPU setup
#SBATCH --partition=pascal
#SBATCH --gres=gpu:4

#Time during which the task will run
#SBATCH --time=72:00:00
#SBATCH --qos=1week

#Paths to STDOUT or STDERR files should be absolute or relative to the current
#working directory
#SBATCH --output=logs/hp-search.o
#SBATCH --error=logs/hp-search.e

#Notify via email when the task ends or fails
#SBATCH --mail-type=END,FAIL,TIME_LIMIT

#This job runs from the current working directory

#Load the required modules
##########################
ml cuDNN/7.6.4.38-gcccuda-2019b
eval "$(conda shell.bash hook)"
conda activate nlp-job-ads

#Run the desired commands
#########################
python scripts/training/hp_search.py
