#!/bin/bash

#Name of the job
#SBATCH --job-name=predict-x28-edu-redux

#Number of CPU cores reserved
#SBATCH --cpus-per-task=4

#Memory reserved per core.
#SBATCH --mem-per-cpu=8G

#GPU setup
#SBATCH --partition=pascal
#SBATCH --gres=gpu:2

#Time during which the task will run
#SBATCH --time=06:00:00
#SBATCH --qos=6hours

#Paths to STDOUT or STDERR files should be absolute or relative to the current
#working directory
#SBATCH --output=logs/predict-x28-edu-redux.o
#SBATCH --error=logs/predict-x28-edu-redux.e

#Notify via email when the task ends or fails
#SBATCH --mail-type=END,FAIL,TIME_LIMIT

#This job runs from the current working directory

#Load the required modules
##########################
ml cuDNN/7.6.4.38-gcccuda-2019b
eval "$(conda shell.bash hook)"
conda activate nlp-job-ads

#Define variables
#################
x28_dir="$HOME/data/x28-job-ads/json"
save_path="$x28_dir/x28_edu_redux_predictions_base_model.csv"
model_path="$HOME/models/nlp-job-ads/gonzpen/gbert-base-ft-edu-redux"
device="0"
num_workers="4"

#Run the desired commands
#########################
python scripts/prediction/predict_x28_edu_redux.py --x28_dir $x28_dir --save_path $save_path --model_path $model_path --device $device --num_workers $num_workers
